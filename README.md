tar xvfz hdf5-1.10.2.tar.gz
mkdir build
cd build
../hdf5-1.10.2/configure --prefix=$(realpath ../hdf5) --disable-fortran --enable-parallel
make
make check -i
# The -i ignores failures and moves on. Tests take a while
# You should look at the chklog files for "FAILURE" 
make install


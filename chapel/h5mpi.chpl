// This is a small enough set of routines, so just do this by hand.
// Nikhil Padmanabhan, Yale 2018

prototype module h5mpi {
  use MPI;
  use h5;
  require "hdf5.h";

  /* Define MPI_Info as an opaque type */
  extern type MPI_Info;
  extern const MPI_INFO_NULL : MPI_Info;


  extern const H5D_ONE_LINK_CHUNK_IO_THRESHOLD : hid_t;
  extern const H5D_MULTI_CHUNK_IO_COL_THRESHOLD : hid_t;

  extern type H5FD_mpio_xfer_t;
  extern const H5FD_MPIO_INDEPENDENT : H5FD_mpio_xfer_t;
  extern const H5FD_MPIO_COLLECTIVE : H5FD_mpio_xfer_t;

  extern type H5FD_mpio_chunk_opt_t;
  extern const H5FD_MPIO_CHUNK_DEFAULT : H5FD_mpio_chunk_opt_t;
  extern const H5FD_MPIO_CHUNK_ONE_IO : H5FD_mpio_chunk_opt_t;
  extern const H5FD_MPIO_CHUNK_MULTI_IO : H5FD_mpio_chunk_opt_t;

  extern type H5FD_mpio_collective_opt_t;
  extern const H5FD_MPIO_COLLECTIVE_IO : H5FD_mpio_collective_opt_t;
  extern const H5FD_MPIO_INDIVIDUAL_IO : H5FD_mpio_collective_opt_t;

  extern var H5FD_mpi_opt_types_g : hbool_t;

  extern proc H5FD_mpio_init() : hid_t;

  extern proc H5Pset_fapl_mpio(fapl_id : hid_t, comm : MPI_Comm, info : MPI_Info) : herr_t;

  extern proc H5Pget_fapl_mpio(fapl_id : hid_t, ref comm : MPI_Comm, ref info : MPI_Info) : herr_t;

  extern proc H5Pset_dxpl_mpio(dxpl_id : hid_t, xfer_mode : H5FD_mpio_xfer_t) : herr_t;

  extern proc H5Pget_dxpl_mpio(dxpl_id : hid_t, ref xfer_mode : H5FD_mpio_xfer_t) : herr_t;

  extern proc H5Pset_dxpl_mpio_collective_opt(dxpl_id : hid_t, opt_mode : H5FD_mpio_collective_opt_t) : herr_t;

  extern proc H5Pset_dxpl_mpio_chunk_opt(dxpl_id : hid_t, opt_mode : H5FD_mpio_chunk_opt_t) : herr_t;

  extern proc H5Pset_dxpl_mpio_chunk_opt_num(dxpl_id : hid_t, num_chunk_per_proc : c_uint) : herr_t;

  extern proc H5Pset_dxpl_mpio_chunk_opt_ratio(dxpl_id : hid_t, percent_num_proc_per_chunk : c_uint) : herr_t;



  // End module 
}

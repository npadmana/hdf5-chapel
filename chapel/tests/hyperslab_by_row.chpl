use h5chapel.Parallel;

config const H5_FILE_NAME="SDS_row.h5";
config const DATASETNAME="IntArray";
config const NX=8;
config const NY=5;

writeln("BEGIN");

// Basic structure can be done on the main locale
{
  var ff = createH5File(H5_FILE_NAME, H5F_ACC_TRUNC);
  var filespace = createSimpleDataSpace({0.. #NX, 0.. #NY});
  var dset = createH5DataSet(ff,DATASETNAME,H5T_NATIVE_INT, filespace);
}

// Do this in SPMD mode
coforall loc in Locales do on loc
{
  // Reopen file
  var ff = openParallelH5File(CHPL_COMM_WORLD,H5_FILE_NAME, H5F_ACC_RDWR);
  MPI.Barrier(CHPL_COMM_WORLD);

  // Get ready for dataset
  const nrows = NX/numLocales;
  var Dom = {here.id*nrows.. #nrows, 0.. #NY};
  var memspace = createSimpleDataSpace(Dom);
  var dset = openH5DataSet(ff,"IntArray");
  var dspace = getDataSpace(dset);
  var status = createHyperSlab(dspace, Dom); 

  var arr : [Dom] int(32);
  arr = (here.id + 10) : int(32);

  writeParallelH5DataSet(CHPL_COMM_WORLD, c_ptrTo(arr[Dom.first]), dset, H5T_NATIVE_INT,memspace.id, dspace.id);
}


// Now read through the file
coforall loc in Locales do on loc
{
  // Reopen file
  var ff = openParallelH5File(CHPL_COMM_WORLD,H5_FILE_NAME, H5F_ACC_RDONLY);
  MPI.Barrier(CHPL_COMM_WORLD);

  // Get ready for dataset
  const nrows = NX/numLocales;
  var Dom = {here.id*nrows.. #nrows, 0.. #NY};
  var memspace = createSimpleDataSpace(Dom);
  var dset = openH5DataSet(ff,"IntArray");
  var dspace = getDataSpace(dset);
  var status = createHyperSlab(dspace, Dom); 

  var arr : [Dom] int(32);

  readParallelH5DataSet(CHPL_COMM_WORLD, c_ptrTo(arr[Dom.first]), dset, H5T_NATIVE_INT,memspace.id, dspace.id);

  // Serialize writes in order
  for iloc in 0.. #numLocales {
    if (here.id == iloc) then writeln(here.id,":",arr);
    MPI.Barrier(CHPL_COMM_WORLD);
  }
}




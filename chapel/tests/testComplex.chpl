use h5chapel;

// Define a complex array
var a1,a2 : [{0.. #3, 0.. #2}] complex;

[(ix,iy) in a1.domain] a1[ix,iy] = 2.0*ix + 3.0i * iy;

var ff = createH5File("complex.h5",H5F_ACC_TRUNC);
var dspace = createSimpleDataSpace(a1.domain);
var dset = createH5DataSet(ff,"/dset",(new H5ComplexType()).hid,dspace);
var status = writeH5DataSet(c_ptrTo(a1), dset, (new H5ComplexType(false)).hid);
status = readH5DataSet(c_ptrTo(a2),dset,(new H5ComplexType(false)).hid);

writeln("BEGIN");

// Validate
for ij in a1.domain {
  var delta = abs(a1[ij]-a2[ij]);
  if (delta > 1.0e-14) then writeln("ERROR! Mismatched in and out");
}



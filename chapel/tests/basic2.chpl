// This routine does the exercises
//   Creating Datasets
//   Writing and Reading Datasets
//   Writing Attributes
//   Creating Groups using Absolute and Relative Names
//   Creating Datasets in Groups
//
// It also uses my Chapel wrappers to avoid some boilerplate code.
//
// Nikhil Padmanabhan
// Yale, 2018

use h5chapel;

proc main() {
  writeln("BEGIN");
  create_dataset();
  write_read_dataset();
  create_dataset_v2();
  write_attribute();
  create_group();
  write_dataset_group();
  hyperslab_example();
}


proc create_dataset() {
  var ff = createH5File("dset.h5",H5F_ACC_TRUNC);
  var dspace = createSimpleDataSpace({-1.. #4, 3.. #6});
  writeln("Dataspace rank :",dspace.rank());
  writeln("Dataspace sizes :",dspace.dims());
  var dset = createH5DataSet(ff,"/dset",H5T_STD_I32BE,dspace);
}

proc write_read_dataset() {
  const D = {0.. #4, 0.. #6};
  var inarr, outarr : [D] int(32); // Since that previous case used 32 bit ints

  [(i,j) in D] outarr[i,j] = (i*6 + j + 1):int(32);

  var ff = openH5File("dset.h5",H5F_ACC_RDWR);
  var dset = openH5DataSet(ff,"/dset");

  var status = writeH5DataSet(c_ptrTo(outarr[0,0]),dset, H5T_NATIVE_INT);
  status = readH5DataSet(c_ptrTo(inarr[0,0]),dset, H5T_NATIVE_INT);

  // Validate
  forall ij in D {
    if inarr[ij] != outarr[ij] then writeln("ERROR! Mismatched in and out");
  }
}

proc create_dataset_v2() {
  const D = {0.. #4, 0.. #6};
  var ff = createH5File("dset2.h5",H5F_ACC_TRUNC);
  var dspace = createSimpleDataSpace({-1.. #4, 3.. #6});
  var outarr : [D] int(32); // Since that previous case used 32 bit ints
  [(i,j) in D] outarr[i,j] = (i*6 + j + 1):int(32);

  var dset = createH5DataSet(ff,"/dset",TypeToH5(outarr.eltType),dspace);
  var status = writeH5DataSet(c_ptrTo(outarr[0,0]),dset, TypeToH5(outarr.eltType,std=false));
}

proc write_attribute() {
  var attrib_data : [0..1] int(32);
  attrib_data[0] = 100;
  attrib_data[1] = 200;

  var ff = openH5File("dset.h5",H5F_ACC_RDWR);
  var dset = openH5DataSet(ff,"/dset");

  // Attributes
  var dspace = createSimpleDataSpace(attrib_data.domain);
  var attr = createH5Attribute(dset,"Units",H5T_STD_I32BE,dspace);
  var status = writeH5Attribute(c_ptrTo(attrib_data[0]),attr,H5T_NATIVE_INT);
}

proc create_group() {
  var ff = createH5File("groups.h5", mode=H5F_ACC_TRUNC);

  var group1 = createH5Group(ff, "/MyGroup");
  var group2 = createH5Group(ff, "/MyGroup/GroupA");
  var group3 = createH5Group(group1,"GroupB");

}

proc write_dataset_group() {
  // Start by just defining the datasets
  var dset1 : [{0..2,0..2}] int(32);
  [(i,j) in dset1.domain] dset1[i,j] = (j+1):int(32);
  var dset2 : [{0.. #2, 0.. #10}] int;
  [(i,j) in dset2.domain] dset2[i,j] = (j+1);

  var ff = openH5File("groups.h5",H5F_ACC_RDWR);
  {
    var dspace = createSimpleDataSpace(dset1.domain);
    var dset = createH5DataSet(ff, "/MyGroup/dset1", H5T_STD_I32BE, dspace);
    var status = writeH5DataSet(c_ptrTo(dset1[0,0]), dset, H5T_NATIVE_INT);
  }

  {
    var grp = openH5Group(ff,"/MyGroup/GroupA");
    var dspace = createSimpleDataSpace(dset2.domain);
    var dset = createH5DataSet(grp, "dset2", H5T_STD_I64BE, dspace);
    var status = writeH5DataSet(c_ptrTo(dset2[0,0]), dset, H5T_NATIVE_LONG);
  }
}

proc hyperslab_example() {
  // Create the file and write basic data to it
  var D = {0.. #8, 0.. #10};
  var arr : [D] int(32);
  {
    var ff = createH5File("subset.h5",H5F_ACC_TRUNC);
    [(i,j) in arr.domain] arr[i,j] = if (j < 5) then 1:int(32) else 2:int(32);
    var dspace = createSimpleDataSpace(arr.domain);
    var dset = createH5DataSet(ff,"IntArray",H5T_STD_I32BE,dspace);
    var status = writeH5DataSet(c_ptrTo(arr[0,0]),dset, H5T_NATIVE_INT);
    writeln(arr);
  }

  // Reopen the file and write a subset of values
  {
    var subD = {1..#3,2..#4};
    var memspace = createSimpleDataSpace(subD);

    var ff = openH5File("subset.h5",H5F_ACC_RDWR);
    var dset = openH5DataSet(ff,"IntArray");
    var dspace = getDataSpace(dset);
    var status = createHyperSlab(dspace, subD);

    var sarr : [subD] int(32);
    sarr = 5:int(32);
    status = writeH5DataSet(c_ptrTo(sarr[sarr.domain.first]),
                            dset, H5T_NATIVE_INT,
                            memspace.id, dspace.id);
    status = readH5DataSet(c_ptrTo(arr[arr.domain.first]),
                           dset, H5T_NATIVE_INT);
    writeln("-----");
    writeln(arr);
  }
}
// Work through the basic examples 
// for HDF5
//
// Nikhil Padmanabhan, Yale
// July 2018

use h5;


// 1. Creating an HDF5 file
proc create_hdf5_file() {
  var file_id = H5Fcreate("file.h5",H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
  var status = H5Fclose(file_id);
}

// 2. Creating a dataset
proc create_dataset() {
  var file_id = H5Fcreate("dset.h5",H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
  defer {
    var status = H5Fclose(file_id);
  }
  var dims : [0..1] hsize_t;
  dims[0] = 4;
  dims[1] = 6;
  var dataspace_id = H5Screate_simple(2, c_ptrTo(dims[0]), nil);
  defer {
    var status = H5Sclose(dataspace_id);
  }
  var dataset_id = H5Dcreate2(file_id, "/dset", H5T_STD_I32BE, dataspace_id,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  var status = H5Dclose(dataset_id);
}

// 2. Creating a dataset
// Creating dataspaces this way is clumsy, so we do directly from a
// a Chapel domain. Note that the dataspace doesn't know about the
// actual limits of the domain.
proc create_dataset_v2() {
  var file_id = H5Fcreate("dset.h5",H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
  defer {
    var status = H5Fclose(file_id);
  }
  var dataspace_id = createSimpleDataSpace({-1.. #4, 3.. #6});
  defer {
    var status = H5Sclose(dataspace_id);
  }
  var dataset_id = H5Dcreate2(file_id, "/dset", H5T_STD_I32BE, dataspace_id,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  var status = H5Dclose(dataset_id);
}

// TODO : As we write more code, simplify further????

// 3. Write to, and read from a dataset.
// NOTE: This uses the dataset from the previous file.
proc write_read_dataset() {
  const D = {0.. #4, 0.. #6};
  var inarr, outarr : [D] int(32); // Since that previous case used 32 bit ints

  [(i,j) in D] outarr[i,j] = (i*6 + j + 1):int(32);

  var file_id = H5Fopen("dset.h5",H5F_ACC_RDWR,H5P_DEFAULT);
  defer {
    var status = H5Fclose(file_id);
  }

  // Open the existing dataset
  var dataset_id = H5Dopen2(file_id, "/dset", H5P_DEFAULT);
  defer {
    var status = H5Dclose(dataset_id);
  }

  /* Write the dataset. */
  var status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, 
                    c_ptrTo(outarr[0,0]));

  status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, 
                   c_ptrTo(inarr[0,0]));


  // Validate
  forall ij in D {
    if inarr[ij] != outarr[ij] then writeln("ERROR! Mismatched in and out");
  }
}

// 4. Write an attribute
proc write_attribute() {
  var attrib_data : [0..1] int(32);
  attrib_data[0] = 100;
  attrib_data[1] = 200;

  var file_id = H5Fopen("dset.h5",H5F_ACC_RDWR,H5P_DEFAULT);
  defer {
    var status = H5Fclose(file_id);
  }

  // Open the existing dataset
  var dataset_id = H5Dopen2(file_id, "/dset", H5P_DEFAULT);
  defer {
    var status = H5Dclose(dataset_id);
  }

  // Create a dataspace
  var dataspace_id = createSimpleDataSpace(attrib_data.domain);
  defer {
    var status = H5Sclose(dataspace_id);
  }

  //Create a dataset attribute.
  var attribute_id = H5Acreate2 (dataset_id, "Units", H5T_STD_I32BE, dataspace_id,
                             H5P_DEFAULT, H5P_DEFAULT);
  defer {
    var status = H5Aclose(attribute_id);
  }
 
  //Write the attribute data.
  var status = H5Awrite(attribute_id, H5T_NATIVE_INT, c_ptrTo(attrib_data[0]));
}
 

// 5. Create a group
proc create_group() {
  var file_id = H5Fcreate("group.h5",H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
  defer {
    var status = H5Fclose(file_id);
  }

  // Create a group named "/MyGroup" in the file.
  var group_id = H5Gcreate2(file_id, "/MyGroup", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
 
  // Close the group.
  var status = H5Gclose(group_id);
}

// 6. Create absolute and relative groups
proc create_group_v2() {
  var file_id = H5Fcreate("group.h5",H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
  defer {
    var status = H5Fclose(file_id);
  }

  // Create a group named "/MyGroup" in the file.
  var group1_id = H5Gcreate2(file_id, "/MyGroup", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  var group2_id = H5Gcreate2(file_id, "/MyGroup/GroupA", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  var group3_id = H5Gcreate2(group1_id, "GroupB", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
 
  // Close the group.
  var status = H5Gclose(group1_id);
  status = H5Gclose(group2_id);
  status = H5Gclose(group3_id);
}

proc main() {
  create_hdf5_file();
  create_dataset();
  create_dataset_v2();
  write_read_dataset();
  write_attribute();
  create_group();
  create_group_v2();
}

proc createSimpleDataSpace(d : domain) where isRectangularDom(d) {
  const rank = d.rank : c_int;
  var dims : [1.. #rank] hsize_t;
  const shape = d.shape;
  for ii in 1.. #rank do dims[ii] = shape(ii):hsize_t;
  return H5Screate_simple(rank, c_ptrTo(dims[1]), nil);
}

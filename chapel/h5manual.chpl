
// Things I added in by hand, because I couldn't
// get c2chapel to correctly deal with these, or
// because they currently got left out.
//
// This is a hodge-podge, and not complete in any
// regard.

extern const H5F_ACC_RDONLY	: c_uint;
extern const H5F_ACC_RDWR	: c_uint;
extern const H5F_ACC_TRUNC	: c_uint;
extern const H5F_ACC_EXCL	: c_uint;
extern const H5F_ACC_CREAT	: c_uint;
extern const H5F_ACC_SWMR_WRITE	: c_uint;
extern const H5F_ACC_SWMR_READ	: c_uint;
extern const H5F_ACC_DEFAULT : c_uint;


// Types

/*
 * These are "standard" types.  For instance, signed (2's complement) and
 * unsigned integers of various sizes and byte orders.
 */
extern const H5T_IEEE_F32LE : hid_t;
extern const H5T_IEEE_F64LE : hid_t;
extern const H5T_IEEE_F32BE : hid_t;
extern const H5T_IEEE_F64BE : hid_t;

extern const H5T_STD_I8BE	: hid_t;	
extern const H5T_STD_I8LE	: hid_t;	
extern const H5T_STD_I16BE	: hid_t;	
extern const H5T_STD_I16LE	: hid_t;	
extern const H5T_STD_I32BE	: hid_t;	
extern const H5T_STD_I32LE	: hid_t;	
extern const H5T_STD_I64BE	: hid_t;	
extern const H5T_STD_I64LE	: hid_t;	
extern const H5T_STD_U8BE	: hid_t;	
extern const H5T_STD_U8LE	: hid_t;	
extern const H5T_STD_U16BE	: hid_t;	
extern const H5T_STD_U16LE	: hid_t;	
extern const H5T_STD_U32BE	: hid_t;	
extern const H5T_STD_U32LE	: hid_t;	
extern const H5T_STD_U64BE	: hid_t;	
extern const H5T_STD_U64LE	: hid_t;	
extern const H5T_STD_B8BE	: hid_t;	
extern const H5T_STD_B8LE	: hid_t;	
extern const H5T_STD_B16BE	: hid_t;	
extern const H5T_STD_B16LE	: hid_t;	
extern const H5T_STD_B32BE	: hid_t;	
extern const H5T_STD_B32LE	: hid_t;	
extern const H5T_STD_B64BE	: hid_t;	
extern const H5T_STD_B64LE	: hid_t;	
extern const H5T_STD_REF_OBJ	       : hid_t; 
extern const H5T_STD_REF_DSETREG    : hid_t; 

/*
 * Types which are particular to Unix.
 */
extern const H5T_UNIX_D32BE	: hid_t;	
extern const H5T_UNIX_D32LE	: hid_t;	
extern const H5T_UNIX_D64BE	: hid_t;	
extern const H5T_UNIX_D64LE	: hid_t;	

/*
 * Types particular to the C language.  String types use `bytes' instead
 * of `bits' as their size.
 */
extern const H5T_C_S1	: hid_t;	

/*
 * Types particular to Fortran.
 */
extern const H5T_FORTRAN_S1	: hid_t;	

/*
 * These types are for Intel CPU's.  They are little endian with IEEE
 * floating point.
 */
extern const H5T_INTEL_I8	: hid_t;	
extern const H5T_INTEL_I16	: hid_t;	
extern const H5T_INTEL_I32	: hid_t;	
extern const H5T_INTEL_I64	: hid_t;	
extern const H5T_INTEL_U8	: hid_t;	
extern const H5T_INTEL_U16	: hid_t;	
extern const H5T_INTEL_U32	: hid_t;	
extern const H5T_INTEL_U64	: hid_t;	
extern const H5T_INTEL_B8	: hid_t;	
extern const H5T_INTEL_B16	: hid_t;	
extern const H5T_INTEL_B32	: hid_t;	
extern const H5T_INTEL_B64	: hid_t;	
extern const H5T_INTEL_F32	: hid_t;	
extern const H5T_INTEL_F64	: hid_t;	

/*
 * These types are for DEC Alpha CPU's.  They are little endian with IEEE
 * floating point.
 */
extern const H5T_ALPHA_I8	: hid_t;	
extern const H5T_ALPHA_I16	: hid_t;	
extern const H5T_ALPHA_I32	: hid_t;	
extern const H5T_ALPHA_I64	: hid_t;	
extern const H5T_ALPHA_U8	: hid_t;	
extern const H5T_ALPHA_U16	: hid_t;	
extern const H5T_ALPHA_U32	: hid_t;	
extern const H5T_ALPHA_U64	: hid_t;	
extern const H5T_ALPHA_B8	: hid_t;	
extern const H5T_ALPHA_B16	: hid_t;	
extern const H5T_ALPHA_B32	: hid_t;	
extern const H5T_ALPHA_B64	: hid_t;	
extern const H5T_ALPHA_F32	: hid_t;	
extern const H5T_ALPHA_F64	: hid_t;	

/*
 * These types are for MIPS cpu's commonly used in SGI systems. They are big
 * endian with IEEE floating point.
 */
extern const H5T_MIPS_I8	: hid_t;	
extern const H5T_MIPS_I16	: hid_t;	
extern const H5T_MIPS_I32	: hid_t;	
extern const H5T_MIPS_I64	: hid_t;	
extern const H5T_MIPS_U8	: hid_t;	
extern const H5T_MIPS_U16	: hid_t;	
extern const H5T_MIPS_U32	: hid_t;	
extern const H5T_MIPS_U64	: hid_t;	
extern const H5T_MIPS_B8	: hid_t;	
extern const H5T_MIPS_B16	: hid_t;	
extern const H5T_MIPS_B32	: hid_t;	
extern const H5T_MIPS_B64	: hid_t;	
extern const H5T_MIPS_F32	: hid_t;	
extern const H5T_MIPS_F64	: hid_t;	

/*
 * The VAX floating point types (i.e. in VAX byte order)
 */
extern const H5T_VAX_F32	: hid_t;	
extern const H5T_VAX_F64	: hid_t;	

/*
 * The predefined native types. These are the types detected by H5detect and
 * they violate the naming scheme a little.  Instead of a class name,
 * precision and byte order as the last component, they have a C-like type
 * name.  If the type begins with `U' then it is the unsigned version of the
 * integer type; other integer types are signed.  The type LLONG corresponds
 * to C's `long long' and LDOUBLE is `long double' (these types might be the
 * same as `LONG' and `DOUBLE' respectively).
 */
extern const H5T_NATIVE_CHAR	: hid_t;	
extern const H5T_NATIVE_SCHAR       : hid_t; 
extern const H5T_NATIVE_UCHAR       : hid_t; 
extern const H5T_NATIVE_SHORT       : hid_t; 
extern const H5T_NATIVE_USHORT      : hid_t; 
extern const H5T_NATIVE_INT         : hid_t; 
extern const H5T_NATIVE_UINT        : hid_t; 
extern const H5T_NATIVE_LONG        : hid_t; 
extern const H5T_NATIVE_ULONG       : hid_t; 
extern const H5T_NATIVE_LLONG       : hid_t; 
extern const H5T_NATIVE_ULLONG      : hid_t; 
extern const H5T_NATIVE_FLOAT       : hid_t; 
extern const H5T_NATIVE_DOUBLE      : hid_t; 
extern const H5T_NATIVE_B8	: hid_t;	
extern const H5T_NATIVE_B16	: hid_t;	
extern const H5T_NATIVE_B32	: hid_t;	
extern const H5T_NATIVE_B64	: hid_t;	
extern const H5T_NATIVE_OPAQUE      : hid_t; 
extern const H5T_NATIVE_HADDR: hid_t;	
extern const H5T_NATIVE_HSIZE: hid_t;	
extern const H5T_NATIVE_HSSIZE: hid_t;	
extern const H5T_NATIVE_HERR	: hid_t;	
extern const H5T_NATIVE_HBOOL: hid_t;	

/* C9x integer types */
extern const H5T_NATIVE_INT8		: hid_t;	
extern const H5T_NATIVE_UINT8	: hid_t;	
extern const H5T_NATIVE_INT_LEAST8	: hid_t;	
extern const H5T_NATIVE_UINT_LEAST8	: hid_t;	
extern const H5T_NATIVE_INT_FAST8 	: hid_t;	
extern const H5T_NATIVE_UINT_FAST8	: hid_t;	

extern const H5T_NATIVE_INT16	: hid_t;	
extern const H5T_NATIVE_UINT16	: hid_t;	
extern const H5T_NATIVE_INT_LEAST16	: hid_t;	
extern const H5T_NATIVE_UINT_LEAST16	: hid_t;	
extern const H5T_NATIVE_INT_FAST16	: hid_t;	
extern const H5T_NATIVE_UINT_FAST16	: hid_t;	

extern const H5T_NATIVE_INT32	: hid_t;	
extern const H5T_NATIVE_UINT32	: hid_t;	
extern const H5T_NATIVE_INT_LEAST32	: hid_t;	
extern const H5T_NATIVE_UINT_LEAST32	: hid_t;	
extern const H5T_NATIVE_INT_FAST32	: hid_t;	
extern const H5T_NATIVE_UINT_FAST32	: hid_t;	

extern const H5T_NATIVE_INT64	: hid_t;	
extern const H5T_NATIVE_UINT64	: hid_t;	
extern const H5T_NATIVE_INT_LEAST64	: hid_t;	
extern const H5T_NATIVE_UINT_LEAST64 : hid_t;	
extern const H5T_NATIVE_INT_FAST64	: hid_t;	
extern const H5T_NATIVE_UINT_FAST64	: hid_t;	


// Definitions from H5Spublic.h
extern const H5S_ALL        : hid_t; 
extern const H5S_UNLIMITED  : hid_t; 


// Definitions from H5Ppublic.h
extern const H5P_ROOT               : hid_t; 
extern const H5P_OBJECT_CREATE      : hid_t; 
extern const H5P_FILE_CREATE        : hid_t; 
extern const H5P_FILE_ACCESS        : hid_t; 
extern const H5P_DATASET_CREATE     : hid_t; 
extern const H5P_DATASET_ACCESS     : hid_t; 
extern const H5P_DATASET_XFER       : hid_t; 
extern const H5P_FILE_MOUNT         : hid_t; 
extern const H5P_GROUP_CREATE       : hid_t; 
extern const H5P_GROUP_ACCESS       : hid_t; 
extern const H5P_DATATYPE_CREATE    : hid_t; 
extern const H5P_DATATYPE_ACCESS    : hid_t; 
extern const H5P_STRING_CREATE      : hid_t; 
extern const H5P_ATTRIBUTE_CREATE   : hid_t; 
extern const H5P_ATTRIBUTE_ACCESS   : hid_t; 
extern const H5P_OBJECT_COPY        : hid_t; 
extern const H5P_LINK_CREATE        : hid_t; 
extern const H5P_LINK_ACCESS        : hid_t; 

/*
 * The library's default property lists
 */
extern const H5P_FILE_CREATE_DEFAULT       : hid_t; 
extern const H5P_FILE_ACCESS_DEFAULT       : hid_t; 
extern const H5P_DATASET_CREATE_DEFAULT    : hid_t; 
extern const H5P_DATASET_ACCESS_DEFAULT    : hid_t; 
extern const H5P_DATASET_XFER_DEFAULT      : hid_t; 
extern const H5P_FILE_MOUNT_DEFAULT        : hid_t; 
extern const H5P_GROUP_CREATE_DEFAULT      : hid_t; 
extern const H5P_GROUP_ACCESS_DEFAULT      : hid_t; 
extern const H5P_DATATYPE_CREATE_DEFAULT   : hid_t; 
extern const H5P_DATATYPE_ACCESS_DEFAULT   : hid_t; 
extern const H5P_ATTRIBUTE_CREATE_DEFAULT  : hid_t; 
extern const H5P_ATTRIBUTE_ACCESS_DEFAULT  : hid_t; 
extern const H5P_OBJECT_COPY_DEFAULT       : hid_t; 
extern const H5P_LINK_CREATE_DEFAULT       : hid_t; 
extern const H5P_LINK_ACCESS_DEFAULT       : hid_t; 

/* Common creation order flags (for links in groups and attributes on objects) */
extern const H5P_CRT_ORDER_TRACKED          : hid_t;
extern const H5P_CRT_ORDER_INDEXED          : hid_t;

/* Default value for all property list classes */
extern const H5P_DEFAULT : hid_t;

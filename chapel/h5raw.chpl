// Generated with c2chapel version 0.1.0

// Header given to c2chapel:
require "hdf5.h";

// Note: Generated with fake std headers

extern proc H5open() : herr_t;

extern proc H5close() : herr_t;

extern proc H5dont_atexit() : herr_t;

extern proc H5garbage_collect() : herr_t;

extern proc H5set_free_list_limits(reg_global_lim : c_int, reg_list_lim : c_int, arr_global_lim : c_int, arr_list_lim : c_int, blk_global_lim : c_int, blk_list_lim : c_int) : herr_t;

extern proc H5get_libversion(ref majnum : c_uint, ref minnum : c_uint, ref relnum : c_uint) : herr_t;

extern proc H5check_version(majnum : c_uint, minnum : c_uint, relnum : c_uint) : herr_t;

extern proc H5is_library_threadsafe(ref is_ts : hbool_t) : herr_t;

extern proc H5free_memory(mem : c_void_ptr) : herr_t;

extern proc H5allocate_memory(size : size_t, clear : hbool_t) : c_void_ptr;

extern proc H5resize_memory(mem : c_void_ptr, size : size_t) : c_void_ptr;

extern proc H5Iregister(type_arg : H5I_type_t, object : c_void_ptr) : hid_t;

extern proc H5Iobject_verify(id : hid_t, id_type : H5I_type_t) : c_void_ptr;

extern proc H5Iremove_verify(id : hid_t, id_type : H5I_type_t) : c_void_ptr;

extern proc H5Iget_type(id : hid_t) : H5I_type_t;

extern proc H5Iget_file_id(id : hid_t) : hid_t;

extern proc H5Iget_name(id : hid_t, name : c_string, size : size_t) : ssize_t;

extern proc H5Iinc_ref(id : hid_t) : c_int;

extern proc H5Idec_ref(id : hid_t) : c_int;

extern proc H5Iget_ref(id : hid_t) : c_int;

extern proc H5Iregister_type(hash_size : size_t, reserved : c_uint, free_func : H5I_free_t) : H5I_type_t;

extern proc H5Iclear_type(type_arg : H5I_type_t, force : hbool_t) : herr_t;

extern proc H5Idestroy_type(type_arg : H5I_type_t) : herr_t;

extern proc H5Iinc_type_ref(type_arg : H5I_type_t) : c_int;

extern proc H5Idec_type_ref(type_arg : H5I_type_t) : c_int;

extern proc H5Iget_type_ref(type_arg : H5I_type_t) : c_int;

extern proc H5Isearch(type_arg : H5I_type_t, func : H5I_search_func_t, key : c_void_ptr) : c_void_ptr;

extern proc H5Inmembers(type_arg : H5I_type_t, ref num_members : hsize_t) : herr_t;

extern proc H5Itype_exists(type_arg : H5I_type_t) : htri_t;

extern proc H5Iis_valid(id : hid_t) : htri_t;

extern var H5T_IEEE_F32BE_g : hid_t;

extern var H5T_IEEE_F32LE_g : hid_t;

extern var H5T_IEEE_F64BE_g : hid_t;

extern var H5T_IEEE_F64LE_g : hid_t;

extern var H5T_STD_I8BE_g : hid_t;

extern var H5T_STD_I8LE_g : hid_t;

extern var H5T_STD_I16BE_g : hid_t;

extern var H5T_STD_I16LE_g : hid_t;

extern var H5T_STD_I32BE_g : hid_t;

extern var H5T_STD_I32LE_g : hid_t;

extern var H5T_STD_I64BE_g : hid_t;

extern var H5T_STD_I64LE_g : hid_t;

extern var H5T_STD_U8BE_g : hid_t;

extern var H5T_STD_U8LE_g : hid_t;

extern var H5T_STD_U16BE_g : hid_t;

extern var H5T_STD_U16LE_g : hid_t;

extern var H5T_STD_U32BE_g : hid_t;

extern var H5T_STD_U32LE_g : hid_t;

extern var H5T_STD_U64BE_g : hid_t;

extern var H5T_STD_U64LE_g : hid_t;

extern var H5T_STD_B8BE_g : hid_t;

extern var H5T_STD_B8LE_g : hid_t;

extern var H5T_STD_B16BE_g : hid_t;

extern var H5T_STD_B16LE_g : hid_t;

extern var H5T_STD_B32BE_g : hid_t;

extern var H5T_STD_B32LE_g : hid_t;

extern var H5T_STD_B64BE_g : hid_t;

extern var H5T_STD_B64LE_g : hid_t;

extern var H5T_STD_REF_OBJ_g : hid_t;

extern var H5T_STD_REF_DSETREG_g : hid_t;

extern var H5T_UNIX_D32BE_g : hid_t;

extern var H5T_UNIX_D32LE_g : hid_t;

extern var H5T_UNIX_D64BE_g : hid_t;

extern var H5T_UNIX_D64LE_g : hid_t;

extern var H5T_C_S1_g : hid_t;

extern var H5T_FORTRAN_S1_g : hid_t;

extern var H5T_VAX_F32_g : hid_t;

extern var H5T_VAX_F64_g : hid_t;

extern var H5T_NATIVE_SCHAR_g : hid_t;

extern var H5T_NATIVE_UCHAR_g : hid_t;

extern var H5T_NATIVE_SHORT_g : hid_t;

extern var H5T_NATIVE_USHORT_g : hid_t;

extern var H5T_NATIVE_INT_g : hid_t;

extern var H5T_NATIVE_UINT_g : hid_t;

extern var H5T_NATIVE_LONG_g : hid_t;

extern var H5T_NATIVE_ULONG_g : hid_t;

extern var H5T_NATIVE_LLONG_g : hid_t;

extern var H5T_NATIVE_ULLONG_g : hid_t;

extern var H5T_NATIVE_FLOAT_g : hid_t;

extern var H5T_NATIVE_DOUBLE_g : hid_t;

extern var H5T_NATIVE_LDOUBLE_g : hid_t;

extern var H5T_NATIVE_B8_g : hid_t;

extern var H5T_NATIVE_B16_g : hid_t;

extern var H5T_NATIVE_B32_g : hid_t;

extern var H5T_NATIVE_B64_g : hid_t;

extern var H5T_NATIVE_OPAQUE_g : hid_t;

extern var H5T_NATIVE_HADDR_g : hid_t;

extern var H5T_NATIVE_HSIZE_g : hid_t;

extern var H5T_NATIVE_HSSIZE_g : hid_t;

extern var H5T_NATIVE_HERR_g : hid_t;

extern var H5T_NATIVE_HBOOL_g : hid_t;

extern var H5T_NATIVE_INT8_g : hid_t;

extern var H5T_NATIVE_UINT8_g : hid_t;

extern var H5T_NATIVE_INT_LEAST8_g : hid_t;

extern var H5T_NATIVE_UINT_LEAST8_g : hid_t;

extern var H5T_NATIVE_INT_FAST8_g : hid_t;

extern var H5T_NATIVE_UINT_FAST8_g : hid_t;

extern var H5T_NATIVE_INT16_g : hid_t;

extern var H5T_NATIVE_UINT16_g : hid_t;

extern var H5T_NATIVE_INT_LEAST16_g : hid_t;

extern var H5T_NATIVE_UINT_LEAST16_g : hid_t;

extern var H5T_NATIVE_INT_FAST16_g : hid_t;

extern var H5T_NATIVE_UINT_FAST16_g : hid_t;

extern var H5T_NATIVE_INT32_g : hid_t;

extern var H5T_NATIVE_UINT32_g : hid_t;

extern var H5T_NATIVE_INT_LEAST32_g : hid_t;

extern var H5T_NATIVE_UINT_LEAST32_g : hid_t;

extern var H5T_NATIVE_INT_FAST32_g : hid_t;

extern var H5T_NATIVE_UINT_FAST32_g : hid_t;

extern var H5T_NATIVE_INT64_g : hid_t;

extern var H5T_NATIVE_UINT64_g : hid_t;

extern var H5T_NATIVE_INT_LEAST64_g : hid_t;

extern var H5T_NATIVE_UINT_LEAST64_g : hid_t;

extern var H5T_NATIVE_INT_FAST64_g : hid_t;

extern var H5T_NATIVE_UINT_FAST64_g : hid_t;

extern proc H5Tcreate(type_arg : H5T_class_t, size : size_t) : hid_t;

extern proc H5Tcopy(type_id : hid_t) : hid_t;

extern proc H5Tclose(type_id : hid_t) : herr_t;

extern proc H5Tequal(type1_id : hid_t, type2_id : hid_t) : htri_t;

extern proc H5Tlock(type_id : hid_t) : herr_t;

extern proc H5Tcommit2(loc_id : hid_t, name : c_string, type_id : hid_t, lcpl_id : hid_t, tcpl_id : hid_t, tapl_id : hid_t) : herr_t;

extern proc H5Topen2(loc_id : hid_t, name : c_string, tapl_id : hid_t) : hid_t;

extern proc H5Tcommit_anon(loc_id : hid_t, type_id : hid_t, tcpl_id : hid_t, tapl_id : hid_t) : herr_t;

extern proc H5Tget_create_plist(type_id : hid_t) : hid_t;

extern proc H5Tcommitted(type_id : hid_t) : htri_t;

extern proc H5Tencode(obj_id : hid_t, buf : c_void_ptr, ref nalloc : size_t) : herr_t;

extern proc H5Tdecode(buf : c_void_ptr) : hid_t;

extern proc H5Tflush(type_id : hid_t) : herr_t;

extern proc H5Trefresh(type_id : hid_t) : herr_t;

extern proc H5Tinsert(parent_id : hid_t, name : c_string, offset : size_t, member_id : hid_t) : herr_t;

extern proc H5Tpack(type_id : hid_t) : herr_t;

extern proc H5Tenum_create(base_id : hid_t) : hid_t;

extern proc H5Tenum_insert(type_arg : hid_t, name : c_string, value : c_void_ptr) : herr_t;

extern proc H5Tenum_nameof(type_arg : hid_t, value : c_void_ptr, name : c_string, size : size_t) : herr_t;

extern proc H5Tenum_valueof(type_arg : hid_t, name : c_string, value : c_void_ptr) : herr_t;

extern proc H5Tvlen_create(base_id : hid_t) : hid_t;

extern proc H5Tarray_create2(base_id : hid_t, ndims : c_uint, dim : c_ptr(hsize_t)) : hid_t;

extern proc H5Tget_array_ndims(type_id : hid_t) : c_int;

extern proc H5Tget_array_dims2(type_id : hid_t, dims : c_ptr(hsize_t)) : c_int;

extern proc H5Tset_tag(type_arg : hid_t, tag : c_string) : herr_t;

extern proc H5Tget_tag(type_arg : hid_t) : c_string;

extern proc H5Tget_super(type_arg : hid_t) : hid_t;

extern proc H5Tget_class(type_id : hid_t) : H5T_class_t;

extern proc H5Tdetect_class(type_id : hid_t, cls : H5T_class_t) : htri_t;

extern proc H5Tget_size(type_id : hid_t) : size_t;

extern proc H5Tget_order(type_id : hid_t) : H5T_order_t;

extern proc H5Tget_precision(type_id : hid_t) : size_t;

extern proc H5Tget_offset(type_id : hid_t) : c_int;

extern proc H5Tget_pad(type_id : hid_t, ref lsb : H5T_pad_t, ref msb : H5T_pad_t) : herr_t;

extern proc H5Tget_sign(type_id : hid_t) : H5T_sign_t;

extern proc H5Tget_fields(type_id : hid_t, ref spos : size_t, ref epos : size_t, ref esize : size_t, ref mpos : size_t, ref msize : size_t) : herr_t;

extern proc H5Tget_ebias(type_id : hid_t) : size_t;

extern proc H5Tget_norm(type_id : hid_t) : H5T_norm_t;

extern proc H5Tget_inpad(type_id : hid_t) : H5T_pad_t;

extern proc H5Tget_strpad(type_id : hid_t) : H5T_str_t;

extern proc H5Tget_nmembers(type_id : hid_t) : c_int;

extern proc H5Tget_member_name(type_id : hid_t, membno : c_uint) : c_string;

extern proc H5Tget_member_index(type_id : hid_t, name : c_string) : c_int;

extern proc H5Tget_member_offset(type_id : hid_t, membno : c_uint) : size_t;

extern proc H5Tget_member_class(type_id : hid_t, membno : c_uint) : H5T_class_t;

extern proc H5Tget_member_type(type_id : hid_t, membno : c_uint) : hid_t;

extern proc H5Tget_member_value(type_id : hid_t, membno : c_uint, value : c_void_ptr) : herr_t;

extern proc H5Tget_cset(type_id : hid_t) : H5T_cset_t;

extern proc H5Tis_variable_str(type_id : hid_t) : htri_t;

extern proc H5Tget_native_type(type_id : hid_t, direction : H5T_direction_t) : hid_t;

extern proc H5Tset_size(type_id : hid_t, size : size_t) : herr_t;

extern proc H5Tset_order(type_id : hid_t, order : H5T_order_t) : herr_t;

extern proc H5Tset_precision(type_id : hid_t, prec : size_t) : herr_t;

extern proc H5Tset_offset(type_id : hid_t, offset : size_t) : herr_t;

extern proc H5Tset_pad(type_id : hid_t, lsb : H5T_pad_t, msb : H5T_pad_t) : herr_t;

extern proc H5Tset_sign(type_id : hid_t, sign : H5T_sign_t) : herr_t;

extern proc H5Tset_fields(type_id : hid_t, spos : size_t, epos : size_t, esize : size_t, mpos : size_t, msize : size_t) : herr_t;

extern proc H5Tset_ebias(type_id : hid_t, ebias : size_t) : herr_t;

extern proc H5Tset_norm(type_id : hid_t, norm : H5T_norm_t) : herr_t;

extern proc H5Tset_inpad(type_id : hid_t, pad : H5T_pad_t) : herr_t;

extern proc H5Tset_cset(type_id : hid_t, cset : H5T_cset_t) : herr_t;

extern proc H5Tset_strpad(type_id : hid_t, strpad : H5T_str_t) : herr_t;

extern proc H5Tregister(pers : H5T_pers_t, name : c_string, src_id : hid_t, dst_id : hid_t, func : H5T_conv_t) : herr_t;

extern proc H5Tunregister(pers : H5T_pers_t, name : c_string, src_id : hid_t, dst_id : hid_t, func : H5T_conv_t) : herr_t;

extern proc H5Tfind(src_id : hid_t, dst_id : hid_t, ref pcdata : c_ptr(H5T_cdata_t)) : H5T_conv_t;

extern proc H5Tcompiler_conv(src_id : hid_t, dst_id : hid_t) : htri_t;

extern proc H5Tconvert(src_id : hid_t, dst_id : hid_t, nelmts : size_t, buf : c_void_ptr, background : c_void_ptr, plist_id : hid_t) : herr_t;

extern proc H5Tcommit1(loc_id : hid_t, name : c_string, type_id : hid_t) : herr_t;

extern proc H5Topen1(loc_id : hid_t, name : c_string) : hid_t;

extern proc H5Tarray_create1(base_id : hid_t, ndims : c_int, dim : c_ptr(hsize_t), perm : c_ptr(c_int)) : hid_t;

extern proc H5Tget_array_dims1(type_id : hid_t, dims : c_ptr(hsize_t), perm : c_ptr(c_int)) : c_int;

extern proc H5Lmove(src_loc : hid_t, src_name : c_string, dst_loc : hid_t, dst_name : c_string, lcpl_id : hid_t, lapl_id : hid_t) : herr_t;

extern proc H5Lcopy(src_loc : hid_t, src_name : c_string, dst_loc : hid_t, dst_name : c_string, lcpl_id : hid_t, lapl_id : hid_t) : herr_t;

extern proc H5Lcreate_hard(cur_loc : hid_t, cur_name : c_string, dst_loc : hid_t, dst_name : c_string, lcpl_id : hid_t, lapl_id : hid_t) : herr_t;

extern proc H5Lcreate_soft(link_target : c_string, link_loc_id : hid_t, link_name : c_string, lcpl_id : hid_t, lapl_id : hid_t) : herr_t;

extern proc H5Ldelete(loc_id : hid_t, name : c_string, lapl_id : hid_t) : herr_t;

extern proc H5Ldelete_by_idx(loc_id : hid_t, group_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, lapl_id : hid_t) : herr_t;

extern proc H5Lget_val(loc_id : hid_t, name : c_string, buf : c_void_ptr, size : size_t, lapl_id : hid_t) : herr_t;

extern proc H5Lget_val_by_idx(loc_id : hid_t, group_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, buf : c_void_ptr, size : size_t, lapl_id : hid_t) : herr_t;

extern proc H5Lexists(loc_id : hid_t, name : c_string, lapl_id : hid_t) : htri_t;

extern proc H5Lget_info(loc_id : hid_t, name : c_string, ref linfo : H5L_info_t, lapl_id : hid_t) : herr_t;

extern proc H5Lget_info_by_idx(loc_id : hid_t, group_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, ref linfo : H5L_info_t, lapl_id : hid_t) : herr_t;

extern proc H5Lget_name_by_idx(loc_id : hid_t, group_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, name : c_string, size : size_t, lapl_id : hid_t) : ssize_t;

extern proc H5Literate(grp_id : hid_t, idx_type : H5_index_t, order : H5_iter_order_t, ref idx : hsize_t, op : H5L_iterate_t, op_data : c_void_ptr) : herr_t;

extern proc H5Literate_by_name(loc_id : hid_t, group_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, ref idx : hsize_t, op : H5L_iterate_t, op_data : c_void_ptr, lapl_id : hid_t) : herr_t;

extern proc H5Lvisit(grp_id : hid_t, idx_type : H5_index_t, order : H5_iter_order_t, op : H5L_iterate_t, op_data : c_void_ptr) : herr_t;

extern proc H5Lvisit_by_name(loc_id : hid_t, group_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, op : H5L_iterate_t, op_data : c_void_ptr, lapl_id : hid_t) : herr_t;

extern proc H5Lcreate_ud(link_loc_id : hid_t, link_name : c_string, link_type : H5L_type_t, udata : c_void_ptr, udata_size : size_t, lcpl_id : hid_t, lapl_id : hid_t) : herr_t;

extern proc H5Lregister(ref cls : H5L_class_t) : herr_t;

extern proc H5Lunregister(id : H5L_type_t) : herr_t;

extern proc H5Lis_registered(id : H5L_type_t) : htri_t;

extern proc H5Lunpack_elink_val(ext_linkval : c_void_ptr, link_size : size_t, ref flags : c_uint, ref filename : c_string, ref obj_path : c_string) : herr_t;

extern proc H5Lcreate_external(file_name : c_string, obj_name : c_string, link_loc_id : hid_t, link_name : c_string, lcpl_id : hid_t, lapl_id : hid_t) : herr_t;

extern proc H5Oopen(loc_id : hid_t, name : c_string, lapl_id : hid_t) : hid_t;

extern proc H5Oopen_by_addr(loc_id : hid_t, addr : haddr_t) : hid_t;

extern proc H5Oopen_by_idx(loc_id : hid_t, group_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, lapl_id : hid_t) : hid_t;

extern proc H5Oexists_by_name(loc_id : hid_t, name : c_string, lapl_id : hid_t) : htri_t;

extern proc H5Oget_info(loc_id : hid_t, ref oinfo : H5O_info_t) : herr_t;

extern proc H5Oget_info_by_name(loc_id : hid_t, name : c_string, ref oinfo : H5O_info_t, lapl_id : hid_t) : herr_t;

extern proc H5Oget_info_by_idx(loc_id : hid_t, group_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, ref oinfo : H5O_info_t, lapl_id : hid_t) : herr_t;

extern proc H5Olink(obj_id : hid_t, new_loc_id : hid_t, new_name : c_string, lcpl_id : hid_t, lapl_id : hid_t) : herr_t;

extern proc H5Oincr_refcount(object_id : hid_t) : herr_t;

extern proc H5Odecr_refcount(object_id : hid_t) : herr_t;

extern proc H5Ocopy(src_loc_id : hid_t, src_name : c_string, dst_loc_id : hid_t, dst_name : c_string, ocpypl_id : hid_t, lcpl_id : hid_t) : herr_t;

extern proc H5Oset_comment(obj_id : hid_t, comment : c_string) : herr_t;

extern proc H5Oset_comment_by_name(loc_id : hid_t, name : c_string, comment : c_string, lapl_id : hid_t) : herr_t;

extern proc H5Oget_comment(obj_id : hid_t, comment : c_string, bufsize : size_t) : ssize_t;

extern proc H5Oget_comment_by_name(loc_id : hid_t, name : c_string, comment : c_string, bufsize : size_t, lapl_id : hid_t) : ssize_t;

extern proc H5Ovisit(obj_id : hid_t, idx_type : H5_index_t, order : H5_iter_order_t, op : H5O_iterate_t, op_data : c_void_ptr) : herr_t;

extern proc H5Ovisit_by_name(loc_id : hid_t, obj_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, op : H5O_iterate_t, op_data : c_void_ptr, lapl_id : hid_t) : herr_t;

extern proc H5Oclose(object_id : hid_t) : herr_t;

extern proc H5Oflush(obj_id : hid_t) : herr_t;

extern proc H5Orefresh(oid : hid_t) : herr_t;

extern proc H5Odisable_mdc_flushes(object_id : hid_t) : herr_t;

extern proc H5Oenable_mdc_flushes(object_id : hid_t) : herr_t;

extern proc H5Oare_mdc_flushes_disabled(object_id : hid_t, ref are_disabled : hbool_t) : herr_t;

extern proc H5Acreate2(loc_id : hid_t, attr_name : c_string, type_id : hid_t, space_id : hid_t, acpl_id : hid_t, aapl_id : hid_t) : hid_t;

extern proc H5Acreate_by_name(loc_id : hid_t, obj_name : c_string, attr_name : c_string, type_id : hid_t, space_id : hid_t, acpl_id : hid_t, aapl_id : hid_t, lapl_id : hid_t) : hid_t;

extern proc H5Aopen(obj_id : hid_t, attr_name : c_string, aapl_id : hid_t) : hid_t;

extern proc H5Aopen_by_name(loc_id : hid_t, obj_name : c_string, attr_name : c_string, aapl_id : hid_t, lapl_id : hid_t) : hid_t;

extern proc H5Aopen_by_idx(loc_id : hid_t, obj_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, aapl_id : hid_t, lapl_id : hid_t) : hid_t;

extern proc H5Awrite(attr_id : hid_t, type_id : hid_t, buf : c_void_ptr) : herr_t;

extern proc H5Aread(attr_id : hid_t, type_id : hid_t, buf : c_void_ptr) : herr_t;

extern proc H5Aclose(attr_id : hid_t) : herr_t;

extern proc H5Aget_space(attr_id : hid_t) : hid_t;

extern proc H5Aget_type(attr_id : hid_t) : hid_t;

extern proc H5Aget_create_plist(attr_id : hid_t) : hid_t;

extern proc H5Aget_name(attr_id : hid_t, buf_size : size_t, buf : c_string) : ssize_t;

extern proc H5Aget_name_by_idx(loc_id : hid_t, obj_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, name : c_string, size : size_t, lapl_id : hid_t) : ssize_t;

extern proc H5Aget_storage_size(attr_id : hid_t) : hsize_t;

extern proc H5Aget_info(attr_id : hid_t, ref ainfo : H5A_info_t) : herr_t;

extern proc H5Aget_info_by_name(loc_id : hid_t, obj_name : c_string, attr_name : c_string, ref ainfo : H5A_info_t, lapl_id : hid_t) : herr_t;

extern proc H5Aget_info_by_idx(loc_id : hid_t, obj_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, ref ainfo : H5A_info_t, lapl_id : hid_t) : herr_t;

extern proc H5Arename(loc_id : hid_t, old_name : c_string, new_name : c_string) : herr_t;

extern proc H5Arename_by_name(loc_id : hid_t, obj_name : c_string, old_attr_name : c_string, new_attr_name : c_string, lapl_id : hid_t) : herr_t;

extern proc H5Aiterate2(loc_id : hid_t, idx_type : H5_index_t, order : H5_iter_order_t, ref idx : hsize_t, op : H5A_operator2_t, op_data : c_void_ptr) : herr_t;

extern proc H5Aiterate_by_name(loc_id : hid_t, obj_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, ref idx : hsize_t, op : H5A_operator2_t, op_data : c_void_ptr, lapd_id : hid_t) : herr_t;

extern proc H5Adelete(loc_id : hid_t, name : c_string) : herr_t;

extern proc H5Adelete_by_name(loc_id : hid_t, obj_name : c_string, attr_name : c_string, lapl_id : hid_t) : herr_t;

extern proc H5Adelete_by_idx(loc_id : hid_t, obj_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, lapl_id : hid_t) : herr_t;

extern proc H5Aexists(obj_id : hid_t, attr_name : c_string) : htri_t;

extern proc H5Aexists_by_name(obj_id : hid_t, obj_name : c_string, attr_name : c_string, lapl_id : hid_t) : htri_t;

extern proc H5Acreate1(loc_id : hid_t, name : c_string, type_id : hid_t, space_id : hid_t, acpl_id : hid_t) : hid_t;

extern proc H5Aopen_name(loc_id : hid_t, name : c_string) : hid_t;

extern proc H5Aopen_idx(loc_id : hid_t, idx : c_uint) : hid_t;

extern proc H5Aget_num_attrs(loc_id : hid_t) : c_int;

extern proc H5Aiterate1(loc_id : hid_t, ref attr_num : c_uint, op : H5A_operator1_t, op_data : c_void_ptr) : herr_t;

extern proc H5Dcreate2(loc_id : hid_t, name : c_string, type_id : hid_t, space_id : hid_t, lcpl_id : hid_t, dcpl_id : hid_t, dapl_id : hid_t) : hid_t;

extern proc H5Dcreate_anon(file_id : hid_t, type_id : hid_t, space_id : hid_t, plist_id : hid_t, dapl_id : hid_t) : hid_t;

extern proc H5Dopen2(file_id : hid_t, name : c_string, dapl_id : hid_t) : hid_t;

extern proc H5Dclose(dset_id : hid_t) : herr_t;

extern proc H5Dget_space(dset_id : hid_t) : hid_t;

extern proc H5Dget_space_status(dset_id : hid_t, ref allocation : H5D_space_status_t) : herr_t;

extern proc H5Dget_type(dset_id : hid_t) : hid_t;

extern proc H5Dget_create_plist(dset_id : hid_t) : hid_t;

extern proc H5Dget_access_plist(dset_id : hid_t) : hid_t;

extern proc H5Dget_storage_size(dset_id : hid_t) : hsize_t;

extern proc H5Dget_chunk_storage_size(dset_id : hid_t, ref offset : hsize_t, ref chunk_bytes : hsize_t) : herr_t;

extern proc H5Dget_offset(dset_id : hid_t) : haddr_t;

extern proc H5Dread(dset_id : hid_t, mem_type_id : hid_t, mem_space_id : hid_t, file_space_id : hid_t, plist_id : hid_t, buf : c_void_ptr) : herr_t;

extern proc H5Dwrite(dset_id : hid_t, mem_type_id : hid_t, mem_space_id : hid_t, file_space_id : hid_t, plist_id : hid_t, buf : c_void_ptr) : herr_t;

extern proc H5Diterate(buf : c_void_ptr, type_id : hid_t, space_id : hid_t, op : H5D_operator_t, operator_data : c_void_ptr) : herr_t;

extern proc H5Dvlen_reclaim(type_id : hid_t, space_id : hid_t, plist_id : hid_t, buf : c_void_ptr) : herr_t;

extern proc H5Dvlen_get_buf_size(dataset_id : hid_t, type_id : hid_t, space_id : hid_t, ref size : hsize_t) : herr_t;

extern proc H5Dfill(fill : c_void_ptr, fill_type : hid_t, buf : c_void_ptr, buf_type : hid_t, space : hid_t) : herr_t;

extern proc H5Dset_extent(dset_id : hid_t, size : c_ptr(hsize_t)) : herr_t;

extern proc H5Dflush(dset_id : hid_t) : herr_t;

extern proc H5Drefresh(dset_id : hid_t) : herr_t;

extern proc H5Dscatter(op : H5D_scatter_func_t, op_data : c_void_ptr, type_id : hid_t, dst_space_id : hid_t, dst_buf : c_void_ptr) : herr_t;

extern proc H5Dgather(src_space_id : hid_t, src_buf : c_void_ptr, type_id : hid_t, dst_buf_size : size_t, dst_buf : c_void_ptr, op : H5D_gather_func_t, op_data : c_void_ptr) : herr_t;

extern proc H5Ddebug(dset_id : hid_t) : herr_t;

extern proc H5Dformat_convert(dset_id : hid_t) : herr_t;

extern proc H5Dget_chunk_index_type(did : hid_t, ref idx_type : H5D_chunk_index_t) : herr_t;

extern proc H5Dcreate1(file_id : hid_t, name : c_string, type_id : hid_t, space_id : hid_t, dcpl_id : hid_t) : hid_t;

extern proc H5Dopen1(file_id : hid_t, name : c_string) : hid_t;

extern proc H5Dextend(dset_id : hid_t, size : c_ptr(hsize_t)) : herr_t;

extern proc H5Fis_hdf5(filename : c_string) : htri_t;

extern proc H5Fcreate(filename : c_string, flags : c_uint, create_plist : hid_t, access_plist : hid_t) : hid_t;

extern proc H5Fopen(filename : c_string, flags : c_uint, access_plist : hid_t) : hid_t;

extern proc H5Freopen(file_id : hid_t) : hid_t;

extern proc H5Fflush(object_id : hid_t, scope : H5F_scope_t) : herr_t;

extern proc H5Fclose(file_id : hid_t) : herr_t;

extern proc H5Fset_mpi_atomicity(file_id : hid_t, flag : hbool_t) : herr_t;

extern proc H5Fget_mpi_atomicity(file_id : hid_t, ref flag : hbool_t) : herr_t;

extern proc H5Gcreate2(loc_id : hid_t, name : c_string, lcpl_id : hid_t, gcpl_id : hid_t, gapl_id : hid_t) : hid_t;

extern proc H5Gcreate_anon(loc_id : hid_t, gcpl_id : hid_t, gapl_id : hid_t) : hid_t;

extern proc H5Gopen2(loc_id : hid_t, name : c_string, gapl_id : hid_t) : hid_t;

extern proc H5Gget_create_plist(group_id : hid_t) : hid_t;

extern proc H5Gget_info(loc_id : hid_t, ref ginfo : H5G_info_t) : herr_t;

extern proc H5Gget_info_by_name(loc_id : hid_t, name : c_string, ref ginfo : H5G_info_t, lapl_id : hid_t) : herr_t;

extern proc H5Gget_info_by_idx(loc_id : hid_t, group_name : c_string, idx_type : H5_index_t, order : H5_iter_order_t, n : hsize_t, ref ginfo : H5G_info_t, lapl_id : hid_t) : herr_t;

extern proc H5Gclose(group_id : hid_t) : herr_t;

extern proc H5Gflush(group_id : hid_t) : herr_t;

extern proc H5Grefresh(group_id : hid_t) : herr_t;

extern proc H5Gcreate1(loc_id : hid_t, name : c_string, size_hint : size_t) : hid_t;

extern proc H5Gopen1(loc_id : hid_t, name : c_string) : hid_t;

extern proc H5Glink(cur_loc_id : hid_t, type_arg : H5L_type_t, cur_name : c_string, new_name : c_string) : herr_t;

extern proc H5Glink2(cur_loc_id : hid_t, cur_name : c_string, type_arg : H5L_type_t, new_loc_id : hid_t, new_name : c_string) : herr_t;

extern proc H5Gmove(src_loc_id : hid_t, src_name : c_string, dst_name : c_string) : herr_t;

extern proc H5Gmove2(src_loc_id : hid_t, src_name : c_string, dst_loc_id : hid_t, dst_name : c_string) : herr_t;

extern proc H5Gunlink(loc_id : hid_t, name : c_string) : herr_t;

extern proc H5Gget_linkval(loc_id : hid_t, name : c_string, size : size_t, buf : c_string) : herr_t;

extern proc H5Gset_comment(loc_id : hid_t, name : c_string, comment : c_string) : herr_t;

extern proc H5Gget_comment(loc_id : hid_t, name : c_string, bufsize : size_t, buf : c_string) : c_int;

extern proc H5Giterate(loc_id : hid_t, name : c_string, ref idx : c_int, op : H5G_iterate_t, op_data : c_void_ptr) : herr_t;

extern proc H5Gget_num_objs(loc_id : hid_t, ref num_objs : hsize_t) : herr_t;

extern proc H5Gget_objinfo(loc_id : hid_t, name : c_string, follow_link : hbool_t, ref statbuf : H5G_stat_t) : herr_t;

extern proc H5Gget_objname_by_idx(loc_id : hid_t, idx : hsize_t, name : c_string, size : size_t) : ssize_t;

extern proc H5Gget_objtype_by_idx(loc_id : hid_t, idx : hsize_t) : H5G_obj_t;

extern record H5FD_t {
  var driver_id : hid_t;
  var cls : c_ptr(H5FD_class_t);
  var fileno : c_ulong;
  var access_flags : c_uint;
  var feature_flags : c_ulong;
  var maxaddr : haddr_t;
  var base_addr : haddr_t;
  var threshold : hsize_t;
  var alignment : hsize_t;
  var paged_aggr : hbool_t;
}

extern proc H5FDregister(ref cls : H5FD_class_t) : hid_t;

extern proc H5FDunregister(driver_id : hid_t) : herr_t;

extern proc H5FDopen(name : c_string, flags : c_uint, fapl_id : hid_t, maxaddr : haddr_t) : c_ptr(H5FD_t);

extern proc H5FDclose(ref file : H5FD_t) : herr_t;

extern proc H5FDcmp(ref f1 : H5FD_t, ref f2 : H5FD_t) : c_int;

extern proc H5FDquery(ref f : H5FD_t, ref flags : c_ulong) : c_int;

extern proc H5FDalloc(ref file : H5FD_t, type_arg : H5FD_mem_t, dxpl_id : hid_t, size : hsize_t) : haddr_t;

extern proc H5FDfree(ref file : H5FD_t, type_arg : H5FD_mem_t, dxpl_id : hid_t, addr : haddr_t, size : hsize_t) : herr_t;

extern proc H5FDget_eoa(ref file : H5FD_t, type_arg : H5FD_mem_t) : haddr_t;

extern proc H5FDset_eoa(ref file : H5FD_t, type_arg : H5FD_mem_t, eoa : haddr_t) : herr_t;

extern proc H5FDget_eof(ref file : H5FD_t, type_arg : H5FD_mem_t) : haddr_t;

extern proc H5FDget_vfd_handle(ref file : H5FD_t, fapl : hid_t, ref file_handle : c_void_ptr) : herr_t;

extern proc H5FDread(ref file : H5FD_t, type_arg : H5FD_mem_t, dxpl_id : hid_t, addr : haddr_t, size : size_t, buf : c_void_ptr) : herr_t;

extern proc H5FDwrite(ref file : H5FD_t, type_arg : H5FD_mem_t, dxpl_id : hid_t, addr : haddr_t, size : size_t, buf : c_void_ptr) : herr_t;

extern proc H5FDflush(ref file : H5FD_t, dxpl_id : hid_t, closing : hbool_t) : herr_t;

extern proc H5FDtruncate(ref file : H5FD_t, dxpl_id : hid_t, closing : hbool_t) : herr_t;

extern proc H5FDlock(ref file : H5FD_t, rw : hbool_t) : herr_t;

extern proc H5FDunlock(ref file : H5FD_t) : herr_t;

extern proc H5FDdriver_query(driver_id : hid_t, ref flags : c_ulong) : herr_t;

extern proc H5Zregister(cls : c_void_ptr) : herr_t;

extern proc H5Zunregister(id : H5Z_filter_t) : herr_t;

extern proc H5Zfilter_avail(id : H5Z_filter_t) : htri_t;

extern proc H5Zget_filter_info(filter : H5Z_filter_t, ref filter_config_flags : c_uint) : herr_t;

extern var H5P_CLS_ROOT_ID_g : hid_t;

extern var H5P_CLS_OBJECT_CREATE_ID_g : hid_t;

extern var H5P_CLS_FILE_CREATE_ID_g : hid_t;

extern var H5P_CLS_FILE_ACCESS_ID_g : hid_t;

extern var H5P_CLS_DATASET_CREATE_ID_g : hid_t;

extern var H5P_CLS_DATASET_ACCESS_ID_g : hid_t;

extern var H5P_CLS_DATASET_XFER_ID_g : hid_t;

extern var H5P_CLS_FILE_MOUNT_ID_g : hid_t;

extern var H5P_CLS_GROUP_CREATE_ID_g : hid_t;

extern var H5P_CLS_GROUP_ACCESS_ID_g : hid_t;

extern var H5P_CLS_DATATYPE_CREATE_ID_g : hid_t;

extern var H5P_CLS_DATATYPE_ACCESS_ID_g : hid_t;

extern var H5P_CLS_STRING_CREATE_ID_g : hid_t;

extern var H5P_CLS_ATTRIBUTE_CREATE_ID_g : hid_t;

extern var H5P_CLS_ATTRIBUTE_ACCESS_ID_g : hid_t;

extern var H5P_CLS_OBJECT_COPY_ID_g : hid_t;

extern var H5P_CLS_LINK_CREATE_ID_g : hid_t;

extern var H5P_CLS_LINK_ACCESS_ID_g : hid_t;

extern var H5P_LST_FILE_CREATE_ID_g : hid_t;

extern var H5P_LST_FILE_ACCESS_ID_g : hid_t;

extern var H5P_LST_DATASET_CREATE_ID_g : hid_t;

extern var H5P_LST_DATASET_ACCESS_ID_g : hid_t;

extern var H5P_LST_DATASET_XFER_ID_g : hid_t;

extern var H5P_LST_FILE_MOUNT_ID_g : hid_t;

extern var H5P_LST_GROUP_CREATE_ID_g : hid_t;

extern var H5P_LST_GROUP_ACCESS_ID_g : hid_t;

extern var H5P_LST_DATATYPE_CREATE_ID_g : hid_t;

extern var H5P_LST_DATATYPE_ACCESS_ID_g : hid_t;

extern var H5P_LST_ATTRIBUTE_CREATE_ID_g : hid_t;

extern var H5P_LST_ATTRIBUTE_ACCESS_ID_g : hid_t;

extern var H5P_LST_OBJECT_COPY_ID_g : hid_t;

extern var H5P_LST_LINK_CREATE_ID_g : hid_t;

extern var H5P_LST_LINK_ACCESS_ID_g : hid_t;

extern proc H5Pcreate_class(parent : hid_t, name : c_string, cls_create : H5P_cls_create_func_t, create_data : c_void_ptr, cls_copy : H5P_cls_copy_func_t, copy_data : c_void_ptr, cls_close : H5P_cls_close_func_t, close_data : c_void_ptr) : hid_t;

extern proc H5Pget_class_name(pclass_id : hid_t) : c_string;

extern proc H5Pcreate(cls_id : hid_t) : hid_t;

extern proc H5Pregister2(cls_id : hid_t, name : c_string, size : size_t, def_value : c_void_ptr, prp_create : H5P_prp_create_func_t, prp_set : H5P_prp_set_func_t, prp_get : H5P_prp_get_func_t, prp_del : H5P_prp_delete_func_t, prp_copy : H5P_prp_copy_func_t, prp_cmp : H5P_prp_compare_func_t, prp_close : H5P_prp_close_func_t) : herr_t;

extern proc H5Pinsert2(plist_id : hid_t, name : c_string, size : size_t, value : c_void_ptr, prp_set : H5P_prp_set_func_t, prp_get : H5P_prp_get_func_t, prp_delete : H5P_prp_delete_func_t, prp_copy : H5P_prp_copy_func_t, prp_cmp : H5P_prp_compare_func_t, prp_close : H5P_prp_close_func_t) : herr_t;

extern proc H5Pset(plist_id : hid_t, name : c_string, value : c_void_ptr) : herr_t;

extern proc H5Pexist(plist_id : hid_t, name : c_string) : htri_t;

extern proc H5Pencode(plist_id : hid_t, buf : c_void_ptr, ref nalloc : size_t) : herr_t;

extern proc H5Pdecode(buf : c_void_ptr) : hid_t;

extern proc H5Pget_size(id : hid_t, name : c_string, ref size : size_t) : herr_t;

extern proc H5Pget_nprops(id : hid_t, ref nprops : size_t) : herr_t;

extern proc H5Pget_class(plist_id : hid_t) : hid_t;

extern proc H5Pget_class_parent(pclass_id : hid_t) : hid_t;

extern proc H5Pget(plist_id : hid_t, name : c_string, value : c_void_ptr) : herr_t;

extern proc H5Pequal(id1 : hid_t, id2 : hid_t) : htri_t;

extern proc H5Pisa_class(plist_id : hid_t, pclass_id : hid_t) : htri_t;

extern proc H5Piterate(id : hid_t, ref idx : c_int, iter_func : H5P_iterate_t, iter_data : c_void_ptr) : c_int;

extern proc H5Pcopy_prop(dst_id : hid_t, src_id : hid_t, name : c_string) : herr_t;

extern proc H5Premove(plist_id : hid_t, name : c_string) : herr_t;

extern proc H5Punregister(pclass_id : hid_t, name : c_string) : herr_t;

extern proc H5Pclose_class(plist_id : hid_t) : herr_t;

extern proc H5Pclose(plist_id : hid_t) : herr_t;

extern proc H5Pcopy(plist_id : hid_t) : hid_t;

extern proc H5Pset_attr_phase_change(plist_id : hid_t, max_compact : c_uint, min_dense : c_uint) : herr_t;

extern proc H5Pget_attr_phase_change(plist_id : hid_t, ref max_compact : c_uint, ref min_dense : c_uint) : herr_t;

extern proc H5Pset_attr_creation_order(plist_id : hid_t, crt_order_flags : c_uint) : herr_t;

extern proc H5Pget_attr_creation_order(plist_id : hid_t, ref crt_order_flags : c_uint) : herr_t;

extern proc H5Pset_obj_track_times(plist_id : hid_t, track_times : hbool_t) : herr_t;

extern proc H5Pget_obj_track_times(plist_id : hid_t, ref track_times : hbool_t) : herr_t;

extern proc H5Pset_deflate(plist_id : hid_t, aggression : c_uint) : herr_t;

extern proc H5Pset_fletcher32(plist_id : hid_t) : herr_t;

extern proc H5Pset_userblock(plist_id : hid_t, size : hsize_t) : herr_t;

extern proc H5Pget_userblock(plist_id : hid_t, ref size : hsize_t) : herr_t;

extern proc H5Pset_sizes(plist_id : hid_t, sizeof_addr : size_t, sizeof_size : size_t) : herr_t;

extern proc H5Pget_sizes(plist_id : hid_t, ref sizeof_addr : size_t, ref sizeof_size : size_t) : herr_t;

extern proc H5Pset_sym_k(plist_id : hid_t, ik : c_uint, lk : c_uint) : herr_t;

extern proc H5Pget_sym_k(plist_id : hid_t, ref ik : c_uint, ref lk : c_uint) : herr_t;

extern proc H5Pset_istore_k(plist_id : hid_t, ik : c_uint) : herr_t;

extern proc H5Pget_istore_k(plist_id : hid_t, ref ik : c_uint) : herr_t;

extern proc H5Pset_shared_mesg_nindexes(plist_id : hid_t, nindexes : c_uint) : herr_t;

extern proc H5Pget_shared_mesg_nindexes(plist_id : hid_t, ref nindexes : c_uint) : herr_t;

extern proc H5Pset_shared_mesg_index(plist_id : hid_t, index_num : c_uint, mesg_type_flags : c_uint, min_mesg_size : c_uint) : herr_t;

extern proc H5Pget_shared_mesg_index(plist_id : hid_t, index_num : c_uint, ref mesg_type_flags : c_uint, ref min_mesg_size : c_uint) : herr_t;

extern proc H5Pset_shared_mesg_phase_change(plist_id : hid_t, max_list : c_uint, min_btree : c_uint) : herr_t;

extern proc H5Pget_shared_mesg_phase_change(plist_id : hid_t, ref max_list : c_uint, ref min_btree : c_uint) : herr_t;

extern proc H5Pset_file_space_strategy(plist_id : hid_t, strategy : H5F_fspace_strategy_t, persist : hbool_t, threshold : hsize_t) : herr_t;

extern proc H5Pget_file_space_strategy(plist_id : hid_t, ref strategy : H5F_fspace_strategy_t, ref persist : hbool_t, ref threshold : hsize_t) : herr_t;

extern proc H5Pset_file_space_page_size(plist_id : hid_t, fsp_size : hsize_t) : herr_t;

extern proc H5Pget_file_space_page_size(plist_id : hid_t, ref fsp_size : hsize_t) : herr_t;

extern proc H5Pset_alignment(fapl_id : hid_t, threshold : hsize_t, alignment : hsize_t) : herr_t;

extern proc H5Pget_alignment(fapl_id : hid_t, ref threshold : hsize_t, ref alignment : hsize_t) : herr_t;

extern proc H5Pset_driver(plist_id : hid_t, driver_id : hid_t, driver_info : c_void_ptr) : herr_t;

extern proc H5Pget_driver(plist_id : hid_t) : hid_t;

extern proc H5Pget_driver_info(plist_id : hid_t) : c_void_ptr;

extern proc H5Pset_family_offset(fapl_id : hid_t, offset : hsize_t) : herr_t;

extern proc H5Pget_family_offset(fapl_id : hid_t, ref offset : hsize_t) : herr_t;

extern proc H5Pset_multi_type(fapl_id : hid_t, type_arg : H5FD_mem_t) : herr_t;

extern proc H5Pget_multi_type(fapl_id : hid_t, ref type_arg : H5FD_mem_t) : herr_t;

extern proc H5Pset_cache(plist_id : hid_t, mdc_nelmts : c_int, rdcc_nslots : size_t, rdcc_nbytes : size_t, rdcc_w0 : c_double) : herr_t;

extern proc H5Pget_cache(plist_id : hid_t, ref mdc_nelmts : c_int, ref rdcc_nslots : size_t, ref rdcc_nbytes : size_t, ref rdcc_w0 : c_double) : herr_t;

extern proc H5Pset_gc_references(fapl_id : hid_t, gc_ref : c_uint) : herr_t;

extern proc H5Pget_gc_references(fapl_id : hid_t, ref gc_ref : c_uint) : herr_t;

extern proc H5Pset_fclose_degree(fapl_id : hid_t, degree : H5F_close_degree_t) : herr_t;

extern proc H5Pget_fclose_degree(fapl_id : hid_t, ref degree : H5F_close_degree_t) : herr_t;

extern proc H5Pset_meta_block_size(fapl_id : hid_t, size : hsize_t) : herr_t;

extern proc H5Pget_meta_block_size(fapl_id : hid_t, ref size : hsize_t) : herr_t;

extern proc H5Pset_sieve_buf_size(fapl_id : hid_t, size : size_t) : herr_t;

extern proc H5Pget_sieve_buf_size(fapl_id : hid_t, ref size : size_t) : herr_t;

extern proc H5Pset_small_data_block_size(fapl_id : hid_t, size : hsize_t) : herr_t;

extern proc H5Pget_small_data_block_size(fapl_id : hid_t, ref size : hsize_t) : herr_t;

extern proc H5Pset_libver_bounds(plist_id : hid_t, low : H5F_libver_t, high : H5F_libver_t) : herr_t;

extern proc H5Pget_libver_bounds(plist_id : hid_t, ref low : H5F_libver_t, ref high : H5F_libver_t) : herr_t;

extern proc H5Pset_elink_file_cache_size(plist_id : hid_t, efc_size : c_uint) : herr_t;

extern proc H5Pget_elink_file_cache_size(plist_id : hid_t, ref efc_size : c_uint) : herr_t;

extern proc H5Pset_file_image(fapl_id : hid_t, buf_ptr : c_void_ptr, buf_len : size_t) : herr_t;

extern proc H5Pget_file_image(fapl_id : hid_t, ref buf_ptr_ptr : c_void_ptr, ref buf_len_ptr : size_t) : herr_t;

extern proc H5Pset_file_image_callbacks(fapl_id : hid_t, ref callbacks_ptr : H5FD_file_image_callbacks_t) : herr_t;

extern proc H5Pget_file_image_callbacks(fapl_id : hid_t, ref callbacks_ptr : H5FD_file_image_callbacks_t) : herr_t;

extern proc H5Pset_core_write_tracking(fapl_id : hid_t, is_enabled : hbool_t, page_size : size_t) : herr_t;

extern proc H5Pget_core_write_tracking(fapl_id : hid_t, ref is_enabled : hbool_t, ref page_size : size_t) : herr_t;

extern proc H5Pset_metadata_read_attempts(plist_id : hid_t, attempts : c_uint) : herr_t;

extern proc H5Pget_metadata_read_attempts(plist_id : hid_t, ref attempts : c_uint) : herr_t;

extern proc H5Pset_mdc_log_options(plist_id : hid_t, is_enabled : hbool_t, location : c_string, start_on_access : hbool_t) : herr_t;

extern proc H5Pget_mdc_log_options(plist_id : hid_t, ref is_enabled : hbool_t, location : c_string, ref location_size : size_t, ref start_on_access : hbool_t) : herr_t;

extern proc H5Pset_evict_on_close(fapl_id : hid_t, evict_on_close : hbool_t) : herr_t;

extern proc H5Pget_evict_on_close(fapl_id : hid_t, ref evict_on_close : hbool_t) : herr_t;

extern proc H5Pset_all_coll_metadata_ops(plist_id : hid_t, is_collective : hbool_t) : herr_t;

extern proc H5Pget_all_coll_metadata_ops(plist_id : hid_t, ref is_collective : hbool_t) : herr_t;

extern proc H5Pset_coll_metadata_write(plist_id : hid_t, is_collective : hbool_t) : herr_t;

extern proc H5Pget_coll_metadata_write(plist_id : hid_t, ref is_collective : hbool_t) : herr_t;

extern proc H5Pset_page_buffer_size(plist_id : hid_t, buf_size : size_t, min_meta_per : c_uint, min_raw_per : c_uint) : herr_t;

extern proc H5Pget_page_buffer_size(plist_id : hid_t, ref buf_size : size_t, ref min_meta_per : c_uint, ref min_raw_per : c_uint) : herr_t;

extern proc H5Pset_layout(plist_id : hid_t, layout : H5D_layout_t) : herr_t;

extern proc H5Pget_layout(plist_id : hid_t) : H5D_layout_t;

extern proc H5Pset_chunk(plist_id : hid_t, ndims : c_int, dim : c_ptr(hsize_t)) : herr_t;

extern proc H5Pget_chunk(plist_id : hid_t, max_ndims : c_int, dim : c_ptr(hsize_t)) : c_int;

extern proc H5Pset_virtual(dcpl_id : hid_t, vspace_id : hid_t, src_file_name : c_string, src_dset_name : c_string, src_space_id : hid_t) : herr_t;

extern proc H5Pget_virtual_count(dcpl_id : hid_t, ref count : size_t) : herr_t;

extern proc H5Pget_virtual_vspace(dcpl_id : hid_t, index_arg : size_t) : hid_t;

extern proc H5Pget_virtual_srcspace(dcpl_id : hid_t, index_arg : size_t) : hid_t;

extern proc H5Pget_virtual_filename(dcpl_id : hid_t, index_arg : size_t, name : c_string, size : size_t) : ssize_t;

extern proc H5Pget_virtual_dsetname(dcpl_id : hid_t, index_arg : size_t, name : c_string, size : size_t) : ssize_t;

extern proc H5Pset_external(plist_id : hid_t, name : c_string, offset : off_t, size : hsize_t) : herr_t;

extern proc H5Pset_chunk_opts(plist_id : hid_t, opts : c_uint) : herr_t;

extern proc H5Pget_chunk_opts(plist_id : hid_t, ref opts : c_uint) : herr_t;

extern proc H5Pget_external_count(plist_id : hid_t) : c_int;

extern proc H5Pget_external(plist_id : hid_t, idx : c_uint, name_size : size_t, name : c_string, ref offset : off_t, ref size : hsize_t) : herr_t;

extern proc H5Pset_szip(plist_id : hid_t, options_mask : c_uint, pixels_per_block : c_uint) : herr_t;

extern proc H5Pset_shuffle(plist_id : hid_t) : herr_t;

extern proc H5Pset_nbit(plist_id : hid_t) : herr_t;

extern proc H5Pset_scaleoffset(plist_id : hid_t, scale_type : H5Z_SO_scale_type_t, scale_factor : c_int) : herr_t;

extern proc H5Pset_fill_value(plist_id : hid_t, type_id : hid_t, value : c_void_ptr) : herr_t;

extern proc H5Pget_fill_value(plist_id : hid_t, type_id : hid_t, value : c_void_ptr) : herr_t;

extern proc H5Pfill_value_defined(plist : hid_t, ref status : H5D_fill_value_t) : herr_t;

extern proc H5Pset_alloc_time(plist_id : hid_t, alloc_time : H5D_alloc_time_t) : herr_t;

extern proc H5Pget_alloc_time(plist_id : hid_t, ref alloc_time : H5D_alloc_time_t) : herr_t;

extern proc H5Pset_fill_time(plist_id : hid_t, fill_time : H5D_fill_time_t) : herr_t;

extern proc H5Pget_fill_time(plist_id : hid_t, ref fill_time : H5D_fill_time_t) : herr_t;

extern proc H5Pset_chunk_cache(dapl_id : hid_t, rdcc_nslots : size_t, rdcc_nbytes : size_t, rdcc_w0 : c_double) : herr_t;

extern proc H5Pget_chunk_cache(dapl_id : hid_t, ref rdcc_nslots : size_t, ref rdcc_nbytes : size_t, ref rdcc_w0 : c_double) : herr_t;

extern proc H5Pset_virtual_view(plist_id : hid_t, view : H5D_vds_view_t) : herr_t;

extern proc H5Pget_virtual_view(plist_id : hid_t, ref view : H5D_vds_view_t) : herr_t;

extern proc H5Pset_virtual_printf_gap(plist_id : hid_t, gap_size : hsize_t) : herr_t;

extern proc H5Pget_virtual_printf_gap(plist_id : hid_t, ref gap_size : hsize_t) : herr_t;

extern proc H5Pset_virtual_prefix(dapl_id : hid_t, prefix : c_string) : herr_t;

extern proc H5Pget_virtual_prefix(dapl_id : hid_t, prefix : c_string, size : size_t) : ssize_t;

extern proc H5Pset_append_flush(plist_id : hid_t, ndims : c_uint, boundary : c_ptr(hsize_t), func : H5D_append_cb_t, udata : c_void_ptr) : herr_t;

extern proc H5Pget_append_flush(plist_id : hid_t, dims : c_uint, boundary : c_ptr(hsize_t), ref func : H5D_append_cb_t, ref udata : c_void_ptr) : herr_t;

extern proc H5Pset_efile_prefix(dapl_id : hid_t, prefix : c_string) : herr_t;

extern proc H5Pget_efile_prefix(dapl_id : hid_t, prefix : c_string, size : size_t) : ssize_t;

extern proc H5Pset_data_transform(plist_id : hid_t, expression : c_string) : herr_t;

extern proc H5Pget_data_transform(plist_id : hid_t, expression : c_string, size : size_t) : ssize_t;

extern proc H5Pset_buffer(plist_id : hid_t, size : size_t, tconv : c_void_ptr, bkg : c_void_ptr) : herr_t;

extern proc H5Pget_buffer(plist_id : hid_t, ref tconv : c_void_ptr, ref bkg : c_void_ptr) : size_t;

extern proc H5Pset_preserve(plist_id : hid_t, status : hbool_t) : herr_t;

extern proc H5Pget_preserve(plist_id : hid_t) : c_int;

extern proc H5Pset_edc_check(plist_id : hid_t, check : H5Z_EDC_t) : herr_t;

extern proc H5Pget_edc_check(plist_id : hid_t) : H5Z_EDC_t;

extern proc H5Pset_filter_callback(plist_id : hid_t, func : H5Z_filter_func_t, op_data : c_void_ptr) : herr_t;

extern proc H5Pset_btree_ratios(plist_id : hid_t, left : c_double, middle : c_double, right : c_double) : herr_t;

extern proc H5Pget_btree_ratios(plist_id : hid_t, ref left : c_double, ref middle : c_double, ref right : c_double) : herr_t;

extern proc H5Pset_hyper_vector_size(fapl_id : hid_t, size : size_t) : herr_t;

extern proc H5Pget_hyper_vector_size(fapl_id : hid_t, ref size : size_t) : herr_t;

extern proc H5Pset_type_conv_cb(dxpl_id : hid_t, op : H5T_conv_except_func_t, operate_data : c_void_ptr) : herr_t;

extern proc H5Pget_type_conv_cb(dxpl_id : hid_t, ref op : H5T_conv_except_func_t, ref operate_data : c_void_ptr) : herr_t;

extern proc H5Pget_mpio_actual_chunk_opt_mode(plist_id : hid_t, ref actual_chunk_opt_mode : H5D_mpio_actual_chunk_opt_mode_t) : herr_t;

extern proc H5Pget_mpio_actual_io_mode(plist_id : hid_t, ref actual_io_mode : H5D_mpio_actual_io_mode_t) : herr_t;

extern proc H5Pget_mpio_no_collective_cause(plist_id : hid_t, ref local_no_collective_cause : uint(32), ref global_no_collective_cause : uint(32)) : herr_t;

extern proc H5Pset_create_intermediate_group(plist_id : hid_t, crt_intmd : c_uint) : herr_t;

extern proc H5Pget_create_intermediate_group(plist_id : hid_t, ref crt_intmd : c_uint) : herr_t;

extern proc H5Pset_local_heap_size_hint(plist_id : hid_t, size_hint : size_t) : herr_t;

extern proc H5Pget_local_heap_size_hint(plist_id : hid_t, ref size_hint : size_t) : herr_t;

extern proc H5Pset_link_phase_change(plist_id : hid_t, max_compact : c_uint, min_dense : c_uint) : herr_t;

extern proc H5Pget_link_phase_change(plist_id : hid_t, ref max_compact : c_uint, ref min_dense : c_uint) : herr_t;

extern proc H5Pset_est_link_info(plist_id : hid_t, est_num_entries : c_uint, est_name_len : c_uint) : herr_t;

extern proc H5Pget_est_link_info(plist_id : hid_t, ref est_num_entries : c_uint, ref est_name_len : c_uint) : herr_t;

extern proc H5Pset_link_creation_order(plist_id : hid_t, crt_order_flags : c_uint) : herr_t;

extern proc H5Pget_link_creation_order(plist_id : hid_t, ref crt_order_flags : c_uint) : herr_t;

extern proc H5Pset_char_encoding(plist_id : hid_t, encoding : H5T_cset_t) : herr_t;

extern proc H5Pget_char_encoding(plist_id : hid_t, ref encoding : H5T_cset_t) : herr_t;

extern proc H5Pset_nlinks(plist_id : hid_t, nlinks : size_t) : herr_t;

extern proc H5Pget_nlinks(plist_id : hid_t, ref nlinks : size_t) : herr_t;

extern proc H5Pset_elink_prefix(plist_id : hid_t, prefix : c_string) : herr_t;

extern proc H5Pget_elink_prefix(plist_id : hid_t, prefix : c_string, size : size_t) : ssize_t;

extern proc H5Pget_elink_fapl(lapl_id : hid_t) : hid_t;

extern proc H5Pset_elink_fapl(lapl_id : hid_t, fapl_id : hid_t) : herr_t;

extern proc H5Pset_elink_acc_flags(lapl_id : hid_t, flags : c_uint) : herr_t;

extern proc H5Pget_elink_acc_flags(lapl_id : hid_t, ref flags : c_uint) : herr_t;

extern proc H5Pset_elink_cb(lapl_id : hid_t, func : H5L_elink_traverse_t, op_data : c_void_ptr) : herr_t;

extern proc H5Pget_elink_cb(lapl_id : hid_t, ref func : H5L_elink_traverse_t, ref op_data : c_void_ptr) : herr_t;

extern proc H5Pset_copy_object(plist_id : hid_t, crt_intmd : c_uint) : herr_t;

extern proc H5Pget_copy_object(plist_id : hid_t, ref crt_intmd : c_uint) : herr_t;

extern proc H5Padd_merge_committed_dtype_path(plist_id : hid_t, path : c_string) : herr_t;

extern proc H5Pfree_merge_committed_dtype_paths(plist_id : hid_t) : herr_t;

extern proc H5Pset_mcdt_search_cb(plist_id : hid_t, func : H5O_mcdt_search_cb_t, op_data : c_void_ptr) : herr_t;

extern proc H5Pget_mcdt_search_cb(plist_id : hid_t, ref func : H5O_mcdt_search_cb_t, ref op_data : c_void_ptr) : herr_t;

extern proc H5Pregister1(cls_id : hid_t, name : c_string, size : size_t, def_value : c_void_ptr, prp_create : H5P_prp_create_func_t, prp_set : H5P_prp_set_func_t, prp_get : H5P_prp_get_func_t, prp_del : H5P_prp_delete_func_t, prp_copy : H5P_prp_copy_func_t, prp_close : H5P_prp_close_func_t) : herr_t;

extern proc H5Pinsert1(plist_id : hid_t, name : c_string, size : size_t, value : c_void_ptr, prp_set : H5P_prp_set_func_t, prp_get : H5P_prp_get_func_t, prp_delete : H5P_prp_delete_func_t, prp_copy : H5P_prp_copy_func_t, prp_close : H5P_prp_close_func_t) : herr_t;

extern proc H5Pget_filter1(plist_id : hid_t, filter : c_uint, ref flags : c_uint, ref cd_nelmts : size_t, cd_values : c_ptr(c_uint), namelen : size_t, name : c_ptr(c_char)) : H5Z_filter_t;

extern proc H5Pget_filter_by_id1(plist_id : hid_t, id : H5Z_filter_t, ref flags : c_uint, ref cd_nelmts : size_t, cd_values : c_ptr(c_uint), namelen : size_t, name : c_ptr(c_char)) : herr_t;

extern proc H5Pget_version(plist_id : hid_t, ref boot : c_uint, ref freelist : c_uint, ref stab : c_uint, ref shhdr : c_uint) : herr_t;

extern proc H5Pset_file_space(plist_id : hid_t, strategy : H5F_file_space_type_t, threshold : hsize_t) : herr_t;

extern proc H5Pget_file_space(plist_id : hid_t, ref strategy : H5F_file_space_type_t, ref threshold : hsize_t) : herr_t;

extern proc H5Screate(type_arg : H5S_class_t) : hid_t;

extern proc H5Screate_simple(rank : c_int, dims : c_ptr(hsize_t), maxdims : c_ptr(hsize_t)) : hid_t;

extern proc H5Sset_extent_simple(space_id : hid_t, rank : c_int, dims : c_ptr(hsize_t), max : c_ptr(hsize_t)) : herr_t;

extern proc H5Scopy(space_id : hid_t) : hid_t;

extern proc H5Sclose(space_id : hid_t) : herr_t;

extern proc H5Sencode(obj_id : hid_t, buf : c_void_ptr, ref nalloc : size_t) : herr_t;

extern proc H5Sdecode(buf : c_void_ptr) : hid_t;

extern proc H5Sget_simple_extent_npoints(space_id : hid_t) : hssize_t;

extern proc H5Sget_simple_extent_ndims(space_id : hid_t) : c_int;

extern proc H5Sget_simple_extent_dims(space_id : hid_t, dims : c_ptr(hsize_t), maxdims : c_ptr(hsize_t)) : c_int;

extern proc H5Sis_simple(space_id : hid_t) : htri_t;

extern proc H5Sget_select_npoints(spaceid : hid_t) : hssize_t;

extern proc H5Sselect_hyperslab(space_id : hid_t, op : H5S_seloper_t, start : c_ptr(hsize_t), _stride : c_ptr(hsize_t), count : c_ptr(hsize_t), _block : c_ptr(hsize_t)) : herr_t;

extern proc H5Sselect_elements(space_id : hid_t, op : H5S_seloper_t, num_elem : size_t, ref coord : hsize_t) : herr_t;

extern proc H5Sget_simple_extent_type(space_id : hid_t) : H5S_class_t;

extern proc H5Sset_extent_none(space_id : hid_t) : herr_t;

extern proc H5Sextent_copy(dst_id : hid_t, src_id : hid_t) : herr_t;

extern proc H5Sextent_equal(sid1 : hid_t, sid2 : hid_t) : htri_t;

extern proc H5Sselect_all(spaceid : hid_t) : herr_t;

extern proc H5Sselect_none(spaceid : hid_t) : herr_t;

extern proc H5Soffset_simple(space_id : hid_t, ref offset : hssize_t) : herr_t;

extern proc H5Sselect_valid(spaceid : hid_t) : htri_t;

extern proc H5Sis_regular_hyperslab(spaceid : hid_t) : htri_t;

extern proc H5Sget_regular_hyperslab(spaceid : hid_t, start : c_ptr(hsize_t), stride : c_ptr(hsize_t), count : c_ptr(hsize_t), block : c_ptr(hsize_t)) : htri_t;

extern proc H5Sget_select_hyper_nblocks(spaceid : hid_t) : hssize_t;

extern proc H5Sget_select_elem_npoints(spaceid : hid_t) : hssize_t;

extern proc H5Sget_select_hyper_blocklist(spaceid : hid_t, startblock : hsize_t, numblocks : hsize_t, buf : c_ptr(hsize_t)) : herr_t;

extern proc H5Sget_select_elem_pointlist(spaceid : hid_t, startpoint : hsize_t, numpoints : hsize_t, buf : c_ptr(hsize_t)) : herr_t;

extern proc H5Sget_select_bounds(spaceid : hid_t, start : c_ptr(hsize_t), end : c_ptr(hsize_t)) : herr_t;

extern proc H5Sget_select_type(spaceid : hid_t) : H5S_sel_type;

// ==== c2chapel typedefs ====

extern record H5A_info_t {
  var corder_valid : hbool_t;
  var corder : H5O_msg_crt_idx_t;
  var cset : H5T_cset_t;
  var data_size : hsize_t;
}

extern type H5A_operator1_t = c_fn_ptr;

extern type H5A_operator2_t = c_fn_ptr;

// H5D_alloc_time_t enum
extern type H5D_alloc_time_t = c_int;
extern const H5D_ALLOC_TIME_ERROR :H5D_alloc_time_t;
extern const H5D_ALLOC_TIME_DEFAULT :H5D_alloc_time_t;
extern const H5D_ALLOC_TIME_EARLY :H5D_alloc_time_t;
extern const H5D_ALLOC_TIME_LATE :H5D_alloc_time_t;
extern const H5D_ALLOC_TIME_INCR :H5D_alloc_time_t;


extern type H5D_append_cb_t = c_fn_ptr;

// H5D_chunk_index_t enum
extern type H5D_chunk_index_t = c_int;
extern const H5D_CHUNK_IDX_BTREE :H5D_chunk_index_t;
extern const H5D_CHUNK_IDX_SINGLE :H5D_chunk_index_t;
extern const H5D_CHUNK_IDX_NONE :H5D_chunk_index_t;
extern const H5D_CHUNK_IDX_FARRAY :H5D_chunk_index_t;
extern const H5D_CHUNK_IDX_EARRAY :H5D_chunk_index_t;
extern const H5D_CHUNK_IDX_BT2 :H5D_chunk_index_t;
extern const H5D_CHUNK_IDX_NTYPES :H5D_chunk_index_t;


// H5D_fill_time_t enum
extern type H5D_fill_time_t = c_int;
extern const H5D_FILL_TIME_ERROR :H5D_fill_time_t;
extern const H5D_FILL_TIME_ALLOC :H5D_fill_time_t;
extern const H5D_FILL_TIME_NEVER :H5D_fill_time_t;
extern const H5D_FILL_TIME_IFSET :H5D_fill_time_t;


// H5D_fill_value_t enum
extern type H5D_fill_value_t = c_int;
extern const H5D_FILL_VALUE_ERROR :H5D_fill_value_t;
extern const H5D_FILL_VALUE_UNDEFINED :H5D_fill_value_t;
extern const H5D_FILL_VALUE_DEFAULT :H5D_fill_value_t;
extern const H5D_FILL_VALUE_USER_DEFINED :H5D_fill_value_t;


extern type H5D_gather_func_t = c_fn_ptr;

// H5D_layout_t enum
extern type H5D_layout_t = c_int;
extern const H5D_LAYOUT_ERROR :H5D_layout_t;
extern const H5D_COMPACT :H5D_layout_t;
extern const H5D_CONTIGUOUS :H5D_layout_t;
extern const H5D_CHUNKED :H5D_layout_t;
extern const H5D_VIRTUAL :H5D_layout_t;
extern const H5D_NLAYOUTS :H5D_layout_t;


// H5D_mpio_actual_chunk_opt_mode_t enum
extern type H5D_mpio_actual_chunk_opt_mode_t = c_int;
extern const H5D_MPIO_NO_CHUNK_OPTIMIZATION :H5D_mpio_actual_chunk_opt_mode_t;
extern const H5D_MPIO_LINK_CHUNK :H5D_mpio_actual_chunk_opt_mode_t;
extern const H5D_MPIO_MULTI_CHUNK :H5D_mpio_actual_chunk_opt_mode_t;


// H5D_mpio_actual_io_mode_t enum
extern type H5D_mpio_actual_io_mode_t = c_int;
extern const H5D_MPIO_NO_COLLECTIVE :H5D_mpio_actual_io_mode_t;
extern const H5D_MPIO_CHUNK_INDEPENDENT :H5D_mpio_actual_io_mode_t;
extern const H5D_MPIO_CHUNK_COLLECTIVE :H5D_mpio_actual_io_mode_t;
extern const H5D_MPIO_CHUNK_MIXED :H5D_mpio_actual_io_mode_t;
extern const H5D_MPIO_CONTIGUOUS_COLLECTIVE :H5D_mpio_actual_io_mode_t;


// H5D_mpio_no_collective_cause_t enum
extern type H5D_mpio_no_collective_cause_t = c_int;
extern const H5D_MPIO_COLLECTIVE :H5D_mpio_no_collective_cause_t;
extern const H5D_MPIO_SET_INDEPENDENT :H5D_mpio_no_collective_cause_t;
extern const H5D_MPIO_DATATYPE_CONVERSION :H5D_mpio_no_collective_cause_t;
extern const H5D_MPIO_DATA_TRANSFORMS :H5D_mpio_no_collective_cause_t;
extern const H5D_MPIO_MPI_OPT_TYPES_ENV_VAR_DISABLED :H5D_mpio_no_collective_cause_t;
extern const H5D_MPIO_NOT_SIMPLE_OR_SCALAR_DATASPACES :H5D_mpio_no_collective_cause_t;
extern const H5D_MPIO_NOT_CONTIGUOUS_OR_CHUNKED_DATASET :H5D_mpio_no_collective_cause_t;
extern const H5D_MPIO_NO_COLLECTIVE_MAX_CAUSE :H5D_mpio_no_collective_cause_t;


extern type H5D_operator_t = c_fn_ptr;

extern type H5D_scatter_func_t = c_fn_ptr;

// H5D_space_status_t enum
extern type H5D_space_status_t = c_int;
extern const H5D_SPACE_STATUS_ERROR :H5D_space_status_t;
extern const H5D_SPACE_STATUS_NOT_ALLOCATED :H5D_space_status_t;
extern const H5D_SPACE_STATUS_PART_ALLOCATED :H5D_space_status_t;
extern const H5D_SPACE_STATUS_ALLOCATED :H5D_space_status_t;


// H5D_vds_view_t enum
extern type H5D_vds_view_t = c_int;
extern const H5D_VDS_ERROR :H5D_vds_view_t;
extern const H5D_VDS_FIRST_MISSING :H5D_vds_view_t;
extern const H5D_VDS_LAST_AVAILABLE :H5D_vds_view_t;


extern record H5FD_class_t {
  var name : c_string;
  var maxaddr : haddr_t;
  var fc_degree : H5F_close_degree_t;
  var terminate : c_fn_ptr;
  var sb_size : c_fn_ptr;
  var sb_encode : c_fn_ptr;
  var sb_decode : c_fn_ptr;
  var fapl_size : size_t;
  var fapl_get : c_fn_ptr;
  var fapl_copy : c_fn_ptr;
  var fapl_free : c_fn_ptr;
  var dxpl_size : size_t;
  var dxpl_copy : c_fn_ptr;
  var dxpl_free : c_fn_ptr;
  var open : c_fn_ptr;
  var close : c_fn_ptr;
  var cmp : c_fn_ptr;
  var query : c_fn_ptr;
  var get_type_map : c_fn_ptr;
  var alloc : c_fn_ptr;
  var free : c_fn_ptr;
  var get_eoa : c_fn_ptr;
  var set_eoa : c_fn_ptr;
  var get_eof : c_fn_ptr;
  var get_handle : c_fn_ptr;
  var read : c_fn_ptr;
  var write : c_fn_ptr;
  var flush : c_fn_ptr;
  var truncate : c_fn_ptr;
  var lock : c_fn_ptr;
  var unlock : c_fn_ptr;
  var fl_map : c_ptr(H5FD_mem_t);
}

extern record H5FD_file_image_callbacks_t {
  var image_malloc : c_fn_ptr;
  var image_memcpy : c_fn_ptr;
  var image_realloc : c_fn_ptr;
  var image_free : c_fn_ptr;
  var udata_copy : c_fn_ptr;
  var udata_free : c_fn_ptr;
  var udata : c_void_ptr;
}

// H5FD_file_image_op_t enum
extern type H5FD_file_image_op_t = c_int;
extern const H5FD_FILE_IMAGE_OP_NO_OP :H5FD_file_image_op_t;
extern const H5FD_FILE_IMAGE_OP_PROPERTY_LIST_SET :H5FD_file_image_op_t;
extern const H5FD_FILE_IMAGE_OP_PROPERTY_LIST_COPY :H5FD_file_image_op_t;
extern const H5FD_FILE_IMAGE_OP_PROPERTY_LIST_GET :H5FD_file_image_op_t;
extern const H5FD_FILE_IMAGE_OP_PROPERTY_LIST_CLOSE :H5FD_file_image_op_t;
extern const H5FD_FILE_IMAGE_OP_FILE_OPEN :H5FD_file_image_op_t;
extern const H5FD_FILE_IMAGE_OP_FILE_RESIZE :H5FD_file_image_op_t;
extern const H5FD_FILE_IMAGE_OP_FILE_CLOSE :H5FD_file_image_op_t;


extern record H5FD_free_t {
  var addr : haddr_t;
  var size : hsize_t;
  var next : c_ptr(H5FD_free_t);
}

// H5FD_mem_t enum
extern type H5FD_mem_t = c_int;


// H5F_close_degree_t enum
extern type H5F_close_degree_t = c_int;
extern const H5F_CLOSE_DEFAULT :H5F_close_degree_t;
extern const H5F_CLOSE_WEAK :H5F_close_degree_t;
extern const H5F_CLOSE_SEMI :H5F_close_degree_t;
extern const H5F_CLOSE_STRONG :H5F_close_degree_t;


// H5F_file_space_type_t enum
extern type H5F_file_space_type_t = c_int;
extern const H5F_FILE_SPACE_DEFAULT :H5F_file_space_type_t;
extern const H5F_FILE_SPACE_ALL_PERSIST :H5F_file_space_type_t;
extern const H5F_FILE_SPACE_ALL :H5F_file_space_type_t;
extern const H5F_FILE_SPACE_AGGR_VFD :H5F_file_space_type_t;
extern const H5F_FILE_SPACE_VFD :H5F_file_space_type_t;
extern const H5F_FILE_SPACE_NTYPES :H5F_file_space_type_t;


// H5F_fspace_strategy_t enum
extern type H5F_fspace_strategy_t = c_int;
extern const H5F_FSPACE_STRATEGY_FSM_AGGR :H5F_fspace_strategy_t;
extern const H5F_FSPACE_STRATEGY_PAGE :H5F_fspace_strategy_t;
extern const H5F_FSPACE_STRATEGY_AGGR :H5F_fspace_strategy_t;
extern const H5F_FSPACE_STRATEGY_NONE :H5F_fspace_strategy_t;
extern const H5F_FSPACE_STRATEGY_NTYPES :H5F_fspace_strategy_t;


// H5F_libver_t enum
extern type H5F_libver_t = c_int;
extern const H5F_LIBVER_ERROR :H5F_libver_t;
extern const H5F_LIBVER_EARLIEST :H5F_libver_t;
extern const H5F_LIBVER_V18 :H5F_libver_t;
extern const H5F_LIBVER_V110 :H5F_libver_t;
extern const H5F_LIBVER_NBOUNDS :H5F_libver_t;


// H5F_mem_t enum
extern type H5F_mem_t = c_int;
extern const H5FD_MEM_NOLIST :H5F_mem_t;
extern const H5FD_MEM_DEFAULT :H5F_mem_t;
extern const H5FD_MEM_SUPER :H5F_mem_t;
extern const H5FD_MEM_BTREE :H5F_mem_t;
extern const H5FD_MEM_DRAW :H5F_mem_t;
extern const H5FD_MEM_GHEAP :H5F_mem_t;
extern const H5FD_MEM_LHEAP :H5F_mem_t;
extern const H5FD_MEM_OHDR :H5F_mem_t;
extern const H5FD_MEM_NTYPES :H5F_mem_t;


extern record H5F_retry_info_t {
  var nbins : c_uint;
  var retries : c_ptr(c_ptr(uint(32)));
}

// H5F_scope_t enum
extern type H5F_scope_t = c_int;
extern const H5F_SCOPE_LOCAL :H5F_scope_t;
extern const H5F_SCOPE_GLOBAL :H5F_scope_t;


extern record H5F_sect_info_t {
  var addr : haddr_t;
  var size : hsize_t;
}

extern record H5G_info_t {
  var storage_type : H5G_storage_type_t;
  var nlinks : hsize_t;
  var max_corder : int(64);
  var mounted : hbool_t;
}

extern type H5G_iterate_t = c_fn_ptr;

// H5G_obj_t enum
extern type H5G_obj_t = c_int;
extern const H5G_UNKNOWN :H5G_obj_t;
extern const H5G_GROUP :H5G_obj_t;
extern const H5G_DATASET :H5G_obj_t;
extern const H5G_TYPE :H5G_obj_t;
extern const H5G_LINK :H5G_obj_t;
extern const H5G_UDLINK :H5G_obj_t;
extern const H5G_RESERVED_5 :H5G_obj_t;
extern const H5G_RESERVED_6 :H5G_obj_t;
extern const H5G_RESERVED_7 :H5G_obj_t;


// Fields omitted because one or more of the identifiers is a Chapel keyword
extern record H5G_stat_t {}

// H5G_storage_type_t enum
extern type H5G_storage_type_t = c_int;
extern const H5G_STORAGE_TYPE_UNKNOWN :H5G_storage_type_t;
extern const H5G_STORAGE_TYPE_SYMBOL_TABLE :H5G_storage_type_t;
extern const H5G_STORAGE_TYPE_COMPACT :H5G_storage_type_t;
extern const H5G_STORAGE_TYPE_DENSE :H5G_storage_type_t;


extern type H5I_free_t = c_fn_ptr;

extern type H5I_search_func_t = c_fn_ptr;

// H5I_type_t enum
extern type H5I_type_t = c_int;
extern const H5I_UNINIT :H5I_type_t;
extern const H5I_BADID :H5I_type_t;
extern const H5I_FILE :H5I_type_t;
extern const H5I_GROUP :H5I_type_t;
extern const H5I_DATATYPE :H5I_type_t;
extern const H5I_DATASPACE :H5I_type_t;
extern const H5I_DATASET :H5I_type_t;
extern const H5I_ATTR :H5I_type_t;
extern const H5I_REFERENCE :H5I_type_t;
extern const H5I_VFL :H5I_type_t;
extern const H5I_GENPROP_CLS :H5I_type_t;
extern const H5I_GENPROP_LST :H5I_type_t;
extern const H5I_ERROR_CLASS :H5I_type_t;
extern const H5I_ERROR_MSG :H5I_type_t;
extern const H5I_ERROR_STACK :H5I_type_t;
extern const H5I_NTYPES :H5I_type_t;


extern record H5L_class_t {
  var version : c_int;
  var id : H5L_type_t;
  var comment : c_string;
  var create_func : H5L_create_func_t;
  var move_func : H5L_move_func_t;
  var copy_func : H5L_copy_func_t;
  var trav_func : H5L_traverse_func_t;
  var del_func : H5L_delete_func_t;
  var query_func : H5L_query_func_t;
}

extern type H5L_copy_func_t = c_fn_ptr;

extern type H5L_create_func_t = c_fn_ptr;

extern type H5L_delete_func_t = c_fn_ptr;

extern type H5L_elink_traverse_t = c_fn_ptr;

// Fields omitted because one or more of the identifiers is a Chapel keyword
extern record H5L_info_t {}

extern type H5L_iterate_t = c_fn_ptr;

extern type H5L_move_func_t = c_fn_ptr;

extern type H5L_query_func_t = c_fn_ptr;

extern type H5L_traverse_func_t = c_fn_ptr;

// H5L_type_t enum
extern type H5L_type_t = c_int;
extern const H5L_TYPE_ERROR :H5L_type_t;
extern const H5L_TYPE_HARD :H5L_type_t;
extern const H5L_TYPE_SOFT :H5L_type_t;
extern const H5L_TYPE_EXTERNAL :H5L_type_t;
extern const H5L_TYPE_MAX :H5L_type_t;


extern record H5O_hdr_info_t {
  var version : c_uint;
  var nmesgs : c_uint;
  var nchunks : c_uint;
  var flags : c_uint;
}

// Fields omitted because one or more of the identifiers is a Chapel keyword
extern record H5O_info_t {}

extern type H5O_iterate_t = c_fn_ptr;

extern type H5O_mcdt_search_cb_t = c_fn_ptr;

// H5O_mcdt_search_ret_t enum
extern type H5O_mcdt_search_ret_t = c_int;
extern const H5O_MCDT_SEARCH_ERROR :H5O_mcdt_search_ret_t;
extern const H5O_MCDT_SEARCH_CONT :H5O_mcdt_search_ret_t;
extern const H5O_MCDT_SEARCH_STOP :H5O_mcdt_search_ret_t;


extern type H5O_msg_crt_idx_t = uint(32);

extern record H5O_stat_t {
  var size : hsize_t;
  var free : hsize_t;
  var nmesgs : c_uint;
  var nchunks : c_uint;
}

// H5O_type_t enum
extern type H5O_type_t = c_int;
extern const H5O_TYPE_UNKNOWN :H5O_type_t;
extern const H5O_TYPE_GROUP :H5O_type_t;
extern const H5O_TYPE_DATASET :H5O_type_t;
extern const H5O_TYPE_NAMED_DATATYPE :H5O_type_t;
extern const H5O_TYPE_NTYPES :H5O_type_t;


extern type H5P_cls_close_func_t = c_fn_ptr;

extern type H5P_cls_copy_func_t = c_fn_ptr;

extern type H5P_cls_create_func_t = c_fn_ptr;

extern type H5P_iterate_t = c_fn_ptr;

extern type H5P_prp_cb1_t = c_fn_ptr;

extern type H5P_prp_cb2_t = c_fn_ptr;

extern type H5P_prp_close_func_t = H5P_prp_cb1_t;

extern type H5P_prp_compare_func_t = c_fn_ptr;

extern type H5P_prp_copy_func_t = H5P_prp_cb1_t;

extern type H5P_prp_create_func_t = H5P_prp_cb1_t;

extern type H5P_prp_delete_func_t = H5P_prp_cb2_t;

extern type H5P_prp_get_func_t = H5P_prp_cb2_t;

extern type H5P_prp_set_func_t = H5P_prp_cb2_t;

// H5S_class_t enum
extern type H5S_class_t = c_int;
extern const H5S_NO_CLASS :H5S_class_t;
extern const H5S_SCALAR :H5S_class_t;
extern const H5S_SIMPLE :H5S_class_t;
extern const H5S_NULL :H5S_class_t;


// H5S_sel_type enum
extern type H5S_sel_type = c_int;
extern const H5S_SEL_ERROR :H5S_sel_type;
extern const H5S_SEL_NONE :H5S_sel_type;
extern const H5S_SEL_POINTS :H5S_sel_type;
extern const H5S_SEL_HYPERSLABS :H5S_sel_type;
extern const H5S_SEL_ALL :H5S_sel_type;
extern const H5S_SEL_N :H5S_sel_type;


// H5S_seloper_t enum
extern type H5S_seloper_t = c_int;
extern const H5S_SELECT_NOOP :H5S_seloper_t;
extern const H5S_SELECT_SET :H5S_seloper_t;
extern const H5S_SELECT_OR :H5S_seloper_t;
extern const H5S_SELECT_AND :H5S_seloper_t;
extern const H5S_SELECT_XOR :H5S_seloper_t;
extern const H5S_SELECT_NOTB :H5S_seloper_t;
extern const H5S_SELECT_NOTA :H5S_seloper_t;
extern const H5S_SELECT_APPEND :H5S_seloper_t;
extern const H5S_SELECT_PREPEND :H5S_seloper_t;
extern const H5S_SELECT_INVALID :H5S_seloper_t;


// H5T_bkg_t enum
extern type H5T_bkg_t = c_int;
extern const H5T_BKG_NO :H5T_bkg_t;
extern const H5T_BKG_TEMP :H5T_bkg_t;
extern const H5T_BKG_YES :H5T_bkg_t;


extern record H5T_cdata_t {
  var command : H5T_cmd_t;
  var need_bkg : H5T_bkg_t;
  var recalc : hbool_t;
  var priv : c_void_ptr;
}

// H5T_class_t enum
extern type H5T_class_t = c_int;
extern const H5T_NO_CLASS :H5T_class_t;
extern const H5T_INTEGER :H5T_class_t;
extern const H5T_FLOAT :H5T_class_t;
extern const H5T_TIME :H5T_class_t;
extern const H5T_STRING :H5T_class_t;
extern const H5T_BITFIELD :H5T_class_t;
extern const H5T_OPAQUE :H5T_class_t;
extern const H5T_COMPOUND :H5T_class_t;
extern const H5T_REFERENCE :H5T_class_t;
extern const H5T_ENUM :H5T_class_t;
extern const H5T_VLEN :H5T_class_t;
extern const H5T_ARRAY :H5T_class_t;
extern const H5T_NCLASSES :H5T_class_t;


// H5T_cmd_t enum
extern type H5T_cmd_t = c_int;
extern const H5T_CONV_INIT :H5T_cmd_t;
extern const H5T_CONV_CONV :H5T_cmd_t;
extern const H5T_CONV_FREE :H5T_cmd_t;


extern type H5T_conv_except_func_t = c_fn_ptr;

// H5T_conv_except_t enum
extern type H5T_conv_except_t = c_int;
extern const H5T_CONV_EXCEPT_RANGE_HI :H5T_conv_except_t;
extern const H5T_CONV_EXCEPT_RANGE_LOW :H5T_conv_except_t;
extern const H5T_CONV_EXCEPT_PRECISION :H5T_conv_except_t;
extern const H5T_CONV_EXCEPT_TRUNCATE :H5T_conv_except_t;
extern const H5T_CONV_EXCEPT_PINF :H5T_conv_except_t;
extern const H5T_CONV_EXCEPT_NINF :H5T_conv_except_t;
extern const H5T_CONV_EXCEPT_NAN :H5T_conv_except_t;


// H5T_conv_ret_t enum
extern type H5T_conv_ret_t = c_int;
extern const H5T_CONV_ABORT :H5T_conv_ret_t;
extern const H5T_CONV_UNHANDLED :H5T_conv_ret_t;
extern const H5T_CONV_HANDLED :H5T_conv_ret_t;


extern type H5T_conv_t = c_fn_ptr;

// H5T_cset_t enum
extern type H5T_cset_t = c_int;
extern const H5T_CSET_ERROR :H5T_cset_t;
extern const H5T_CSET_ASCII :H5T_cset_t;
extern const H5T_CSET_UTF8 :H5T_cset_t;
extern const H5T_CSET_RESERVED_2 :H5T_cset_t;
extern const H5T_CSET_RESERVED_3 :H5T_cset_t;
extern const H5T_CSET_RESERVED_4 :H5T_cset_t;
extern const H5T_CSET_RESERVED_5 :H5T_cset_t;
extern const H5T_CSET_RESERVED_6 :H5T_cset_t;
extern const H5T_CSET_RESERVED_7 :H5T_cset_t;
extern const H5T_CSET_RESERVED_8 :H5T_cset_t;
extern const H5T_CSET_RESERVED_9 :H5T_cset_t;
extern const H5T_CSET_RESERVED_10 :H5T_cset_t;
extern const H5T_CSET_RESERVED_11 :H5T_cset_t;
extern const H5T_CSET_RESERVED_12 :H5T_cset_t;
extern const H5T_CSET_RESERVED_13 :H5T_cset_t;
extern const H5T_CSET_RESERVED_14 :H5T_cset_t;
extern const H5T_CSET_RESERVED_15 :H5T_cset_t;


// H5T_direction_t enum
extern type H5T_direction_t = c_int;
extern const H5T_DIR_DEFAULT :H5T_direction_t;
extern const H5T_DIR_ASCEND :H5T_direction_t;
extern const H5T_DIR_DESCEND :H5T_direction_t;


// H5T_norm_t enum
extern type H5T_norm_t = c_int;
extern const H5T_NORM_ERROR :H5T_norm_t;
extern const H5T_NORM_IMPLIED :H5T_norm_t;
extern const H5T_NORM_MSBSET :H5T_norm_t;
extern const H5T_NORM_NONE :H5T_norm_t;


// H5T_order_t enum
extern type H5T_order_t = c_int;
extern const H5T_ORDER_ERROR :H5T_order_t;
extern const H5T_ORDER_LE :H5T_order_t;
extern const H5T_ORDER_BE :H5T_order_t;
extern const H5T_ORDER_VAX :H5T_order_t;
extern const H5T_ORDER_MIXED :H5T_order_t;
extern const H5T_ORDER_NONE :H5T_order_t;


// H5T_pad_t enum
extern type H5T_pad_t = c_int;
extern const H5T_PAD_ERROR :H5T_pad_t;
extern const H5T_PAD_ZERO :H5T_pad_t;
extern const H5T_PAD_ONE :H5T_pad_t;
extern const H5T_PAD_BACKGROUND :H5T_pad_t;
extern const H5T_NPAD :H5T_pad_t;


// H5T_pers_t enum
extern type H5T_pers_t = c_int;
extern const H5T_PERS_DONTCARE :H5T_pers_t;
extern const H5T_PERS_HARD :H5T_pers_t;
extern const H5T_PERS_SOFT :H5T_pers_t;


// H5T_sign_t enum
extern type H5T_sign_t = c_int;
extern const H5T_SGN_ERROR :H5T_sign_t;
extern const H5T_SGN_NONE :H5T_sign_t;
extern const H5T_SGN_2 :H5T_sign_t;
extern const H5T_NSGN :H5T_sign_t;


// H5T_str_t enum
extern type H5T_str_t = c_int;
extern const H5T_STR_ERROR :H5T_str_t;
extern const H5T_STR_NULLTERM :H5T_str_t;
extern const H5T_STR_NULLPAD :H5T_str_t;
extern const H5T_STR_SPACEPAD :H5T_str_t;
extern const H5T_STR_RESERVED_3 :H5T_str_t;
extern const H5T_STR_RESERVED_4 :H5T_str_t;
extern const H5T_STR_RESERVED_5 :H5T_str_t;
extern const H5T_STR_RESERVED_6 :H5T_str_t;
extern const H5T_STR_RESERVED_7 :H5T_str_t;
extern const H5T_STR_RESERVED_8 :H5T_str_t;
extern const H5T_STR_RESERVED_9 :H5T_str_t;
extern const H5T_STR_RESERVED_10 :H5T_str_t;
extern const H5T_STR_RESERVED_11 :H5T_str_t;
extern const H5T_STR_RESERVED_12 :H5T_str_t;
extern const H5T_STR_RESERVED_13 :H5T_str_t;
extern const H5T_STR_RESERVED_14 :H5T_str_t;
extern const H5T_STR_RESERVED_15 :H5T_str_t;


// H5Z_EDC_t enum
extern type H5Z_EDC_t = c_int;
extern const H5Z_ERROR_EDC :H5Z_EDC_t;
extern const H5Z_DISABLE_EDC :H5Z_EDC_t;
extern const H5Z_ENABLE_EDC :H5Z_EDC_t;
extern const H5Z_NO_EDC :H5Z_EDC_t;


// H5Z_SO_scale_type_t enum
extern type H5Z_SO_scale_type_t = c_int;
extern const H5Z_SO_FLOAT_DSCALE :H5Z_SO_scale_type_t;
extern const H5Z_SO_FLOAT_ESCALE :H5Z_SO_scale_type_t;
extern const H5Z_SO_INT :H5Z_SO_scale_type_t;


extern type H5Z_can_apply_func_t = c_fn_ptr;

// H5Z_cb_return_t enum
extern type H5Z_cb_return_t = c_int;
extern const H5Z_CB_ERROR :H5Z_cb_return_t;
extern const H5Z_CB_FAIL :H5Z_cb_return_t;
extern const H5Z_CB_CONT :H5Z_cb_return_t;
extern const H5Z_CB_NO :H5Z_cb_return_t;


extern record H5Z_cb_t {
  var func : H5Z_filter_func_t;
  var op_data : c_void_ptr;
}

extern record H5Z_class1_t {
  var id : H5Z_filter_t;
  var name : c_string;
  var can_apply : H5Z_can_apply_func_t;
  var set_local : H5Z_set_local_func_t;
  var filter : H5Z_func_t;
}

extern record H5Z_class2_t {
  var version : c_int;
  var id : H5Z_filter_t;
  var encoder_present : c_uint;
  var decoder_present : c_uint;
  var name : c_string;
  var can_apply : H5Z_can_apply_func_t;
  var set_local : H5Z_set_local_func_t;
  var filter : H5Z_func_t;
}

extern type H5Z_filter_func_t = c_fn_ptr;

extern type H5Z_filter_t = c_int;

extern type H5Z_func_t = c_fn_ptr;

extern type H5Z_set_local_func_t = c_fn_ptr;

extern record H5_ih_info_t {
  var index_size : hsize_t;
  var heap_size : hsize_t;
}

// H5_index_t enum
extern type H5_index_t = c_int;
extern const H5_INDEX_UNKNOWN :H5_index_t;
extern const H5_INDEX_NAME :H5_index_t;
extern const H5_INDEX_CRT_ORDER :H5_index_t;
extern const H5_INDEX_N :H5_index_t;


// H5_iter_order_t enum
extern type H5_iter_order_t = c_int;
extern const H5_ITER_UNKNOWN :H5_iter_order_t;
extern const H5_ITER_INC :H5_iter_order_t;
extern const H5_ITER_DEC :H5_iter_order_t;
extern const H5_ITER_NATIVE :H5_iter_order_t;
extern const H5_ITER_N :H5_iter_order_t;






extern type haddr_t = c_ulong;

extern type hbool_t = bool;

extern type herr_t = c_int;

extern type hid_t = int(64);

extern type hsize_t = c_ulonglong;

extern type hssize_t = c_longlong;

extern type htri_t = c_int;

extern record hvl_t {
  var len : size_t;
  var p : c_void_ptr;
}

// c2chapel thinks these typedefs are from the fake headers:
/*
extern type FILE = c_int;

extern type _LOCK_RECURSIVE_T = c_int;

extern type _LOCK_T = c_int;

extern type __FILE = c_int;

extern type __ULong = c_int;

extern type __builtin_va_list = c_int;

extern type __dev_t = c_int;

extern type __gid_t = c_int;

extern type __gnuc_va_list = c_int;

extern type __int16_t = c_int;

extern type __int32_t = c_int;

extern type __int64_t = c_int;

extern type __int8_t = c_int;

extern type __int_least16_t = c_int;

extern type __int_least32_t = c_int;

extern type __loff_t = c_int;

extern type __off_t = c_int;

extern type __pid_t = c_int;

extern type __s16 = c_int;

extern type __s32 = c_int;

extern type __s64 = c_int;

extern type __s8 = c_int;

extern type __sigset_t = c_int;

extern type __tzinfo_type = c_int;

extern type __tzrule_type = c_int;

extern type __u16 = c_int;

extern type __u32 = c_int;

extern type __u64 = c_int;

extern type __u8 = c_int;

extern type __uid_t = c_int;

extern type __uint16_t = c_int;

extern type __uint32_t = c_int;

extern type __uint64_t = c_int;

extern type __uint8_t = c_int;

extern type __uint_least16_t = c_int;

extern type __uint_least32_t = c_int;

extern type _flock_t = c_int;

extern type _fpos_t = c_int;

extern type _iconv_t = c_int;

extern type _mbstate_t = c_int;

extern type _off64_t = c_int;

extern type _off_t = c_int;

extern type _sig_func_ptr = c_int;

extern type _ssize_t = c_int;

extern type _types_fd_set = c_int;

extern type bool = _Bool;

extern type caddr_t = c_int;

extern type clock_t = c_int;

extern type clockid_t = c_int;

extern type cookie_close_function_t = c_int;

extern type cookie_io_functions_t = c_int;

extern type cookie_read_function_t = c_int;

extern type cookie_seek_function_t = c_int;

extern type cookie_write_function_t = c_int;

extern type daddr_t = c_int;

extern type dev_t = c_int;

extern type div_t = c_int;

extern type fd_mask = c_int;

extern type fpos_t = c_int;

extern type gid_t = c_int;

extern type ino_t = c_int;

extern type int16_t = c_int;

extern type int32_t = c_int;

extern type int64_t = c_int;

extern type int8_t = c_int;

extern type int_fast16_t = c_int;

extern type int_fast32_t = c_int;

extern type int_fast64_t = c_int;

extern type int_fast8_t = c_int;

extern type int_least16_t = c_int;

extern type int_least32_t = c_int;

extern type int_least64_t = c_int;

extern type int_least8_t = c_int;

extern type intmax_t = c_int;

extern type intptr_t = c_int;

extern type jmp_buf = c_int;

extern type key_t = c_int;

extern type ldiv_t = c_int;

extern type lldiv_t = c_int;

extern type mbstate_t = c_int;

extern type mode_t = c_int;

extern type nlink_t = c_int;

extern type off_t = c_int;

extern type pid_t = c_int;

extern type pthread_attr_t = c_int;

extern type pthread_barrier_t = c_int;

extern type pthread_barrierattr_t = c_int;

extern type pthread_cond_t = c_int;

extern type pthread_condattr_t = c_int;

extern type pthread_key_t = c_int;

extern type pthread_mutex_t = c_int;

extern type pthread_mutexattr_t = c_int;

extern type pthread_once_t = c_int;

extern type pthread_rwlock_t = c_int;

extern type pthread_rwlockattr_t = c_int;

extern type pthread_spinlock_t = c_int;

extern type pthread_t = c_int;

extern type ptrdiff_t = c_int;

extern type rlim_t = c_int;

extern type sa_family_t = c_int;

extern type sem_t = c_int;

extern type sig_atomic_t = c_int;

extern type siginfo_t = c_int;

extern type sigjmp_buf = c_int;

extern type sigset_t = c_int;

extern type size_t = c_int;

extern type ssize_t = c_int;

extern type stack_t = c_int;

extern type suseconds_t = c_int;

extern type time_t = c_int;

extern type timer_t = c_int;

extern type u_char = c_int;

extern type u_int = c_int;

extern type u_long = c_int;

extern type u_short = c_int;

extern type uid_t = c_int;

extern type uint = c_int;

extern type uint16_t = c_int;

extern type uint32_t = c_int;

extern type uint64_t = c_int;

extern type uint8_t = c_int;

extern type uint_fast16_t = c_int;

extern type uint_fast32_t = c_int;

extern type uint_fast64_t = c_int;

extern type uint_fast8_t = c_int;

extern type uint_least16_t = c_int;

extern type uint_least32_t = c_int;

extern type uint_least64_t = c_int;

extern type uint_least8_t = c_int;

extern type uintmax_t = c_int;

extern type uintptr_t = c_int;

extern type useconds_t = c_int;

extern type ushort = c_int;

extern type va_list = c_int;

extern type wchar_t = c_int;

extern type wint_t = c_int;

extern type z_stream = c_int;

*/

// Things I added in by hand, because I couldn't
// get c2chapel to correctly deal with these, or
// because they currently got left out.
//
// This is a hodge-podge, and not complete in any
// regard.

extern const H5F_ACC_RDONLY	: c_uint;
extern const H5F_ACC_RDWR	: c_uint;
extern const H5F_ACC_TRUNC	: c_uint;
extern const H5F_ACC_EXCL	: c_uint;
extern const H5F_ACC_CREAT	: c_uint;
extern const H5F_ACC_SWMR_WRITE	: c_uint;
extern const H5F_ACC_SWMR_READ	: c_uint;
extern const H5F_ACC_DEFAULT : c_uint;


// Types

/*
 * These are "standard" types.  For instance, signed (2's complement) and
 * unsigned integers of various sizes and byte orders.
 */
extern const H5T_STD_I8BE	: hid_t;	
extern const H5T_STD_I8LE	: hid_t;	
extern const H5T_STD_I16BE	: hid_t;	
extern const H5T_STD_I16LE	: hid_t;	
extern const H5T_STD_I32BE	: hid_t;	
extern const H5T_STD_I32LE	: hid_t;	
extern const H5T_STD_I64BE	: hid_t;	
extern const H5T_STD_I64LE	: hid_t;	
extern const H5T_STD_U8BE	: hid_t;	
extern const H5T_STD_U8LE	: hid_t;	
extern const H5T_STD_U16BE	: hid_t;	
extern const H5T_STD_U16LE	: hid_t;	
extern const H5T_STD_U32BE	: hid_t;	
extern const H5T_STD_U32LE	: hid_t;	
extern const H5T_STD_U64BE	: hid_t;	
extern const H5T_STD_U64LE	: hid_t;	
extern const H5T_STD_B8BE	: hid_t;	
extern const H5T_STD_B8LE	: hid_t;	
extern const H5T_STD_B16BE	: hid_t;	
extern const H5T_STD_B16LE	: hid_t;	
extern const H5T_STD_B32BE	: hid_t;	
extern const H5T_STD_B32LE	: hid_t;	
extern const H5T_STD_B64BE	: hid_t;	
extern const H5T_STD_B64LE	: hid_t;	
extern const H5T_STD_REF_OBJ	       : hid_t; 
extern const H5T_STD_REF_DSETREG    : hid_t; 

/*
 * Types which are particular to Unix.
 */
extern const H5T_UNIX_D32BE	: hid_t;	
extern const H5T_UNIX_D32LE	: hid_t;	
extern const H5T_UNIX_D64BE	: hid_t;	
extern const H5T_UNIX_D64LE	: hid_t;	

/*
 * Types particular to the C language.  String types use `bytes' instead
 * of `bits' as their size.
 */
extern const H5T_C_S1	: hid_t;	

/*
 * Types particular to Fortran.
 */
extern const H5T_FORTRAN_S1	: hid_t;	

/*
 * These types are for Intel CPU's.  They are little endian with IEEE
 * floating point.
 */
extern const H5T_INTEL_I8	: hid_t;	
extern const H5T_INTEL_I16	: hid_t;	
extern const H5T_INTEL_I32	: hid_t;	
extern const H5T_INTEL_I64	: hid_t;	
extern const H5T_INTEL_U8	: hid_t;	
extern const H5T_INTEL_U16	: hid_t;	
extern const H5T_INTEL_U32	: hid_t;	
extern const H5T_INTEL_U64	: hid_t;	
extern const H5T_INTEL_B8	: hid_t;	
extern const H5T_INTEL_B16	: hid_t;	
extern const H5T_INTEL_B32	: hid_t;	
extern const H5T_INTEL_B64	: hid_t;	
extern const H5T_INTEL_F32	: hid_t;	
extern const H5T_INTEL_F64	: hid_t;	

/*
 * These types are for DEC Alpha CPU's.  They are little endian with IEEE
 * floating point.
 */
extern const H5T_ALPHA_I8	: hid_t;	
extern const H5T_ALPHA_I16	: hid_t;	
extern const H5T_ALPHA_I32	: hid_t;	
extern const H5T_ALPHA_I64	: hid_t;	
extern const H5T_ALPHA_U8	: hid_t;	
extern const H5T_ALPHA_U16	: hid_t;	
extern const H5T_ALPHA_U32	: hid_t;	
extern const H5T_ALPHA_U64	: hid_t;	
extern const H5T_ALPHA_B8	: hid_t;	
extern const H5T_ALPHA_B16	: hid_t;	
extern const H5T_ALPHA_B32	: hid_t;	
extern const H5T_ALPHA_B64	: hid_t;	
extern const H5T_ALPHA_F32	: hid_t;	
extern const H5T_ALPHA_F64	: hid_t;	

/*
 * These types are for MIPS cpu's commonly used in SGI systems. They are big
 * endian with IEEE floating point.
 */
extern const H5T_MIPS_I8	: hid_t;	
extern const H5T_MIPS_I16	: hid_t;	
extern const H5T_MIPS_I32	: hid_t;	
extern const H5T_MIPS_I64	: hid_t;	
extern const H5T_MIPS_U8	: hid_t;	
extern const H5T_MIPS_U16	: hid_t;	
extern const H5T_MIPS_U32	: hid_t;	
extern const H5T_MIPS_U64	: hid_t;	
extern const H5T_MIPS_B8	: hid_t;	
extern const H5T_MIPS_B16	: hid_t;	
extern const H5T_MIPS_B32	: hid_t;	
extern const H5T_MIPS_B64	: hid_t;	
extern const H5T_MIPS_F32	: hid_t;	
extern const H5T_MIPS_F64	: hid_t;	

/*
 * The VAX floating point types (i.e. in VAX byte order)
 */
extern const H5T_VAX_F32	: hid_t;	
extern const H5T_VAX_F64	: hid_t;	

/*
 * The predefined native types. These are the types detected by H5detect and
 * they violate the naming scheme a little.  Instead of a class name,
 * precision and byte order as the last component, they have a C-like type
 * name.  If the type begins with `U' then it is the unsigned version of the
 * integer type; other integer types are signed.  The type LLONG corresponds
 * to C's `long long' and LDOUBLE is `long double' (these types might be the
 * same as `LONG' and `DOUBLE' respectively).
 */
extern const H5T_NATIVE_CHAR	: hid_t;	
extern const H5T_NATIVE_SCHAR       : hid_t; 
extern const H5T_NATIVE_UCHAR       : hid_t; 
extern const H5T_NATIVE_SHORT       : hid_t; 
extern const H5T_NATIVE_USHORT      : hid_t; 
extern const H5T_NATIVE_INT         : hid_t; 
extern const H5T_NATIVE_UINT        : hid_t; 
extern const H5T_NATIVE_LONG        : hid_t; 
extern const H5T_NATIVE_ULONG       : hid_t; 
extern const H5T_NATIVE_LLONG       : hid_t; 
extern const H5T_NATIVE_ULLONG      : hid_t; 
extern const H5T_NATIVE_FLOAT       : hid_t; 
extern const H5T_NATIVE_DOUBLE      : hid_t; 
extern const H5T_NATIVE_B8	: hid_t;	
extern const H5T_NATIVE_B16	: hid_t;	
extern const H5T_NATIVE_B32	: hid_t;	
extern const H5T_NATIVE_B64	: hid_t;	
extern const H5T_NATIVE_OPAQUE      : hid_t; 
extern const H5T_NATIVE_HADDR: hid_t;	
extern const H5T_NATIVE_HSIZE: hid_t;	
extern const H5T_NATIVE_HSSIZE: hid_t;	
extern const H5T_NATIVE_HERR	: hid_t;	
extern const H5T_NATIVE_HBOOL: hid_t;	

/* C9x integer types */
extern const H5T_NATIVE_INT8		: hid_t;	
extern const H5T_NATIVE_UINT8	: hid_t;	
extern const H5T_NATIVE_INT_LEAST8	: hid_t;	
extern const H5T_NATIVE_UINT_LEAST8	: hid_t;	
extern const H5T_NATIVE_INT_FAST8 	: hid_t;	
extern const H5T_NATIVE_UINT_FAST8	: hid_t;	

extern const H5T_NATIVE_INT16	: hid_t;	
extern const H5T_NATIVE_UINT16	: hid_t;	
extern const H5T_NATIVE_INT_LEAST16	: hid_t;	
extern const H5T_NATIVE_UINT_LEAST16	: hid_t;	
extern const H5T_NATIVE_INT_FAST16	: hid_t;	
extern const H5T_NATIVE_UINT_FAST16	: hid_t;	

extern const H5T_NATIVE_INT32	: hid_t;	
extern const H5T_NATIVE_UINT32	: hid_t;	
extern const H5T_NATIVE_INT_LEAST32	: hid_t;	
extern const H5T_NATIVE_UINT_LEAST32	: hid_t;	
extern const H5T_NATIVE_INT_FAST32	: hid_t;	
extern const H5T_NATIVE_UINT_FAST32	: hid_t;	

extern const H5T_NATIVE_INT64	: hid_t;	
extern const H5T_NATIVE_UINT64	: hid_t;	
extern const H5T_NATIVE_INT_LEAST64	: hid_t;	
extern const H5T_NATIVE_UINT_LEAST64 : hid_t;	
extern const H5T_NATIVE_INT_FAST64	: hid_t;	
extern const H5T_NATIVE_UINT_FAST64	: hid_t;	


// Definitions from H5Spublic.h
extern const H5S_ALL        : hid_t; 
extern const H5S_UNLIMITED  : hid_t; 


// Definitions from H5Ppublic.h
extern const H5P_ROOT               : hid_t; 
extern const H5P_OBJECT_CREATE      : hid_t; 
extern const H5P_FILE_CREATE        : hid_t; 
extern const H5P_FILE_ACCESS        : hid_t; 
extern const H5P_DATASET_CREATE     : hid_t; 
extern const H5P_DATASET_ACCESS     : hid_t; 
extern const H5P_DATASET_XFER       : hid_t; 
extern const H5P_FILE_MOUNT         : hid_t; 
extern const H5P_GROUP_CREATE       : hid_t; 
extern const H5P_GROUP_ACCESS       : hid_t; 
extern const H5P_DATATYPE_CREATE    : hid_t; 
extern const H5P_DATATYPE_ACCESS    : hid_t; 
extern const H5P_STRING_CREATE      : hid_t; 
extern const H5P_ATTRIBUTE_CREATE   : hid_t; 
extern const H5P_ATTRIBUTE_ACCESS   : hid_t; 
extern const H5P_OBJECT_COPY        : hid_t; 
extern const H5P_LINK_CREATE        : hid_t; 
extern const H5P_LINK_ACCESS        : hid_t; 

/*
 * The library's default property lists
 */
extern const H5P_FILE_CREATE_DEFAULT       : hid_t; 
extern const H5P_FILE_ACCESS_DEFAULT       : hid_t; 
extern const H5P_DATASET_CREATE_DEFAULT    : hid_t; 
extern const H5P_DATASET_ACCESS_DEFAULT    : hid_t; 
extern const H5P_DATASET_XFER_DEFAULT      : hid_t; 
extern const H5P_FILE_MOUNT_DEFAULT        : hid_t; 
extern const H5P_GROUP_CREATE_DEFAULT      : hid_t; 
extern const H5P_GROUP_ACCESS_DEFAULT      : hid_t; 
extern const H5P_DATATYPE_CREATE_DEFAULT   : hid_t; 
extern const H5P_DATATYPE_ACCESS_DEFAULT   : hid_t; 
extern const H5P_ATTRIBUTE_CREATE_DEFAULT  : hid_t; 
extern const H5P_ATTRIBUTE_ACCESS_DEFAULT  : hid_t; 
extern const H5P_OBJECT_COPY_DEFAULT       : hid_t; 
extern const H5P_LINK_CREATE_DEFAULT       : hid_t; 
extern const H5P_LINK_ACCESS_DEFAULT       : hid_t; 

/* Common creation order flags (for links in groups and attributes on objects) */
extern const H5P_CRT_ORDER_TRACKED          : hid_t;
extern const H5P_CRT_ORDER_INDEXED          : hid_t;

/* Default value for all property list classes */
extern const H5P_DEFAULT : hid_t;

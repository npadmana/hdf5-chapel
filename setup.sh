#!/bin/bash

if [ "x$HDF5_DIR" == x ]; then
    ME=$(realpath ${BASH_SOURCE[0]})
    export HDF5_DIR=$(dirname $ME)/hdf5
    export CHPL_MODULE_PATH=$(dirname $ME)/chapel:${CHPL_MODULE_PATH}
    export LD_LIBRARY_PATH=${HDF5_DIR}/lib:$LD_LIBRARY_PATH
    export LIBRARY_PATH=${HDF5_DIR}/lib:$LIBRARY_PATH
    export C_INCLUDE_PATH=${HDF5_DIR}/include:$C_INCLUDE_PATH
    export INCLUDE_PATH=${HDF5_DIR}/lib:$INCLUDE_PATH
    export PATH=${HDF5_DIR}/bin:$PATH
fi

